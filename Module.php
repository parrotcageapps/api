<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API;

use Parrot\API\Resource\Controller\ResourceController;
use Parrot\API\Resource\Listener\ResourceListenerAggregate;
use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\Controller\ControllerManager;
use Zend\Mvc\ResponseSender\SendResponseEvent;

/**
 * Class Module
 * @package Parrot\API
 */
class Module implements BootstrapListenerInterface, ConfigProviderInterface, AutoloaderProviderInterface
{
    /**
     * Listen to the bootstrap event
     *
     * @param EventInterface $e
     * @return array
     */
    public function onBootstrap(EventInterface $e)
    {
        $app            = $e->getTarget();
        $sm             = $app->getServiceManager();
        $events         = $app->getEventManager();

        $events->attach($sm->get('Parrot\API\Problem\Listener\ProblemListener'));
        $events->attach('render', array($this, 'onRender'), 100);

        $events->attachAggregate($sm->get('Parrot\API\Resource\Listener\ResponseCacheListener'));

        $events->getSharedManager()->attachAggregate($sm->get('Parrot\API\Resource\Listener\ContentValidationListener'));
        $events->getSharedManager()->attachAggregate($sm->get('Parrot\API\Resource\Listener\RestParameterListener'));
        $events->getSharedManager()->attachAggregate($sm->get('Parrot\API\Resource\Listener\MethodNotAllowedListener'));
        $events->getSharedManager()->attachAggregate(new ResourceListenerAggregate());

        $sendResponseListener = $sm->get('SendResponseListener');
        $sendResponseListener->getEventManager()->attach(
            SendResponseEvent::EVENT_SEND_RESPONSE,
            $sm->get('Parrot\API\Problem\Listener\SendProblemResponseListener'),
            -500
        );
    }

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/Parrot/API',
                ),
            ),
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to seed
     * such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getControllerConfig()
    {
        return array(
            'factories' => array(
                'Parrot\API\Resource\Controller\Resource' => function (ControllerManager $cm) {
                    $controller = new ResourceController();
                    $sm         = $cm->getServiceLocator();

                    $controller->setResourceLocatorService($sm->get('Parrot\API\Service\ResourceLocator'));

                    return $controller;
                },
            ),
        );
    }

    /**
     * Listener for the render event
     *
     * Attaches a rendering/response strategy to the View.
     *
     * @param  \Zend\Mvc\MvcEvent $e
     */
    public function onRender($e)
    {
        $app      = $e->getTarget();
        $services = $app->getServiceManager();

        if ($services->has('View')) {
            $view   = $services->get('View');
            $events = $view->getEventManager();

            $events->attach($services->get('Parrot\API\Problem\View\Strategy\ProblemStrategy'), 400);
        }
    }
}

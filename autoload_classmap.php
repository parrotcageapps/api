<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
    'Parrot\API\Module'                                             => __DIR__ . '/Module.php',
    'Parrot\API\Problem\Exception\DomainException'                  => __DIR__ . '/src/Parrot/API/Problem/Exception/DomainException.php',
    'Parrot\API\Problem\Exception\ExceptionInterface'               => __DIR__ . '/src/Parrot/API/Problem/Exception/ExceptionInterface.php',
    'Parrot\API\Problem\Exception\InvalidArgumentException'         => __DIR__ . '/src/Parrot/API/Problem/Exception/InvalidArgumentException.php',
    'Parrot\API\Problem\Exception\ProblemExceptionInterface'        => __DIR__ . '/src/Parrot/API/Problem/Exception/ProblemExceptionInterface.php',
    'Parrot\API\Problem\Factory\ProblemListenerFactory'             => __DIR__ . '/src/Parrot/API/Problem/Factory/ProblemListenerFactory.php',
    'Parrot\API\Problem\Factory\ProblemRendererFactory'             => __DIR__ . '/src/Parrot/API/Problem/Factory/ProblemRendererFactory.php',
    'Parrot\API\Problem\Factory\ProblemStrategyFactory'             => __DIR__ . '/src/Parrot/API/Problem/Factory/ProblemStrategyFactory.php',
    'Parrot\API\Problem\Factory\RenderErrorListenerFactory'         => __DIR__ . '/src/Parrot/API/Problem/Factory/RenderErrorListenerFactory.php',
    'Parrot\API\Problem\Factory\SendProblemResponseListenerFactory' => __DIR__ . '/src/Parrot/API/Problem/Factory/SendProblemResponseListenerFactory.php',
    'Parrot\API\Problem\Listener\ProblemListener'                   => __DIR__ . '/src/Parrot/API/Problem/Listener/ProblemListener.php',
    'Parrot\API\Problem\Listener\RenderErrorListener'               => __DIR__ . '/src/Parrot/API/Problem/Listener/RenderErrorListener.php',
    'Parrot\API\Problem\Listener\SendProblemResponseListener'       => __DIR__ . '/src/Parrot/API/Problem/Listener/SendProblemResponseListener.php',
    'Parrot\API\Problem\Problem'                                    => __DIR__ . '/src/Parrot/API/Problem/Problem.php',
    'Parrot\API\Problem\Response\ProblemResponse'                   => __DIR__ . '/src/Parrot/API/Problem/Response/ProblemResponse.php',
    'Parrot\API\Problem\View\Model\ProblemModel'                    => __DIR__ . '/src/Parrot/API/Problem/View/Model/ProblemModel.php',
    'Parrot\API\Problem\View\Renderer\ProblemRenderer'              => __DIR__ . '/src/Parrot/API/Problem/View/Renderer/ProblemRenderer.php',
    'Parrot\API\Problem\View\Strategy\ProblemStrategy'              => __DIR__ . '/src/Parrot/API/Problem/View/Strategy/ProblemStrategy.php',
    'Parrot\API\Resource\Backend\BaseBackend'                       => __DIR__ . '/src/Parrot/API/Resource/Backend/BaseBackend.php',
    'Parrot\API\Resource\Backend\Collection\Collection'             => __DIR__ . '/src/Parrot/API/Resource/Backend/Collection/Collection.php',
    'Parrot\API\Resource\Backend\Collection\Pagination'             => __DIR__ . '/src/Parrot/API/Resource/Backend/Collection/Pagination.php',
    'Parrot\API\Resource\Backend\Doctrine\DocumentBackend'          => __DIR__ . '/src/Parrot/API/Resource/Backend/Doctrine/DocumentBackend.php',
    'Parrot\API\Resource\Backend\Doctrine\EntityBackend'            => __DIR__ . '/src/Parrot/API/Resource/Backend/Doctrine/EntityBackend.php',
    'Parrot\API\Resource\Backend\EmbeddedResourcesInterface'        => __DIR__ . '/src/Parrot/API/Resource/Backend/EmbeddedResourcesInterface.php',
    'Parrot\API\Resource\Backend\EmbeddedResourceTrait'             => __DIR__ . '/src/Parrot/API/Resource/Backend/EmbeddedResourceTrait.php',
    'Parrot\API\Resource\Backend\Guzzle\GuzzleBackend'              => __DIR__ . '/src/Parrot/API/Resource/Backend/Guzzle/GuzzleBackend.php',
    'Parrot\API\Resource\Backend\Guzzle\GuzzleClientAwareInterface' => __DIR__ . '/src/Parrot/API/Resource/Backend/Guzzle/GuzzleClientAwareInterface.php',
    'Parrot\API\Resource\Backend\Guzzle\GuzzleClientAwareTrait'     => __DIR__ . '/src/Parrot/API/Resource/Backend/Guzzle/GuzzleClientAwareTrait.php',
    'Parrot\API\Resource\Backend\Guzzle\RequestMap'                 => __DIR__ . '/src/Parrot/API/Resource/Backend/Guzzle/RequestMap.php',
    'Parrot\API\Resource\Backend\PassiveResourceInterface'          => __DIR__ . '/src/Parrot/API/Resource/Backend/PassiveResourceInterface.php',
    'Parrot\API\Resource\Backend\ResourceInterface'                 => __DIR__ . '/src/Parrot/API/Resource/Backend/ResourceInterface.php',
    'Parrot\API\Resource\Backend\ResourceTrait'                     => __DIR__ . '/src/Parrot/API/Resource/Backend/ResourceTrait.php',
    'Parrot\API\Resource\Cache\CacheOptions'                        => __DIR__ . '/src/Parrot/API/Resource/Cache/CacheOptions.php',
    'Parrot\API\Resource\Controller\ResourceController'             => __DIR__ . '/src/Parrot/API/Resource/Controller/ResourceController.php',
    'Parrot\API\Resource\Exception\ExceptionInterface'              => __DIR__ . '/src/Parrot/API/Resource/Exception/ExceptionInterface.php',
    'Parrot\API\Resource\Exception\ResourceNotImplementedException' => __DIR__ . '/src/Parrot/API/Resource/Exception/ResourceNotImplementedException.php',
    'Parrot\API\Resource\Factory\ResourceCacheFactory'              => __DIR__ . '/src/Parrot/API/Resource/Factory/ResourceCacheFactory.php',
    'Parrot\API\Resource\Factory\ResourceFactory'                   => __DIR__ . '/src/Parrot/API/Resource/Factory/ResourceFactory.php',
    'Parrot\API\Resource\Factory\ResourceJsonHalStrategyFactory'    => __DIR__ . '/src/Parrot/API/Resource/Factory/ResourceJsonHalStrategyFactory.php',
    'Parrot\API\Resource\Factory\ResourceLocatorFactory'            => __DIR__ . '/src/Parrot/API/Resource/Factory/ResourceLocatorFactory.php',
    'Parrot\API\Resource\Factory\ResponseCacheListenerFactory'      => __DIR__ . '/src/Parrot/API/Resource/Factory/ResponseCacheListenerFactory.php',
    'Parrot\API\Resource\InputFilter\Resource'                      => __DIR__ . '/src/Parrot/API/Resource/InputFilter/Resource.php',
    'Parrot\API\Resource\Link\Link'                                 => __DIR__ . '/src/Parrot/API/Resource/Link/Link.php',
    'Parrot\API\Resource\Link\LinkCollection'                       => __DIR__ . '/src/Parrot/API/Resource/Link/LinkCollection.php',
    'Parrot\API\Resource\Link\LinkCollectionAwareInterface'         => __DIR__ . '/src/Parrot/API/Resource/Link/LinkCollectionAwareInterface.php',
    'Parrot\API\Resource\Link\LinkCollectionAwareTrait'             => __DIR__ . '/src/Parrot/API/Resource/Link/LinkCollectionAwareTrait.php',
    'Parrot\API\Resource\Listener\ContentValidationListener'        => __DIR__ . '/src/Parrot/API/Resource/Listener/ContentValidationListener.php',
    'Parrot\API\Resource\Listener\MethodNotAllowedListener'         => __DIR__ . '/src/Parrot/API/Resource/Listener/MethodNotAllowedListener.php',
    'Parrot\API\Resource\Listener\ResponseCacheListenerAggregate'   => __DIR__ . '/src/Parrot/API/Resource/Listener/ResponseCacheListenerAggregate.php',
    'Parrot\API\Resource\Listener\RestParameterListener'            => __DIR__ . '/src/Parrot/API/Resource/Listener/RestParameterListener.php',
    'Parrot\API\Resource\Pluralization'                             => __DIR__ . '/src/Parrot/API/Resource/Pluralization.php',
    'Parrot\API\Resource\Renderer\Plugin\JMS\JsonPlugin'            => __DIR__ . '/src/Parrot/API/Resource/Renderer/Plugin/JMS/JsonPlugin.php',
    'Parrot\API\Resource\Renderer\Plugin\PluginAwareInterface'      => __DIR__ . '/src/Parrot/API/Resource/Renderer/Plugin/PluginAwareInterface.php',
    'Parrot\API\Resource\Renderer\Plugin\PluginAwareTrait'          => __DIR__ . '/src/Parrot/API/Resource/Renderer/Plugin/PluginAwareTrait.php',
    'Parrot\API\Resource\Renderer\Plugin\PluginInterface'           => __DIR__ . '/src/Parrot/API/Resource/Renderer/Plugin/PluginInterface.php',
    'Parrot\API\Resource\Resource'                                  => __DIR__ . '/src/Parrot/API/Resource/Resource.php',
    'Parrot\API\Resource\ResourceEvent'                             => __DIR__ . '/src/Parrot/API/Resource/ResourceEvent.php',
    'Parrot\API\Resource\ResourceInterface'                         => __DIR__ . '/src/Parrot/API/Resource/ResourceInterface.php',
    'Parrot\API\Resource\Service\ResourceLocator'                   => __DIR__ . '/src/Parrot/API/Resource/Service/ResourceLocator.php',
    'Parrot\API\Resource\Service\ResourceLocatorAwareInterface'     => __DIR__ . '/src/Parrot/API/Resource/Service/ResourceLocatorAwareInterface.php',
    'Parrot\API\Resource\Service\ResourceLocatorAwareTrait'         => __DIR__ . '/src/Parrot/API/Resource/Service/ResourceLocatorAwareTrait.php',
    'Parrot\API\Resource\View\Model\ResourceJsonModel'              => __DIR__ . '/src/Parrot/API/Resource/View/Model/ResourceJsonModel.php',
    'Parrot\API\Resource\View\Renderer\ResourceJsonRenderer'        => __DIR__ . '/src/Parrot/API/Resource/View/Renderer/ResourceJsonRenderer.php',
    'Parrot\API\Resource\View\Strategy\ResourceJsonHalStrategy'     => __DIR__ . '/src/Parrot/API/Resource/View/Strategy/ResourceJsonHalStrategy.php',
    'Parrot\API\Resource\View\Strategy\ResourceJsonStrategy'        => __DIR__ . '/src/Parrot/API/Resource/View/Strategy/ResourceJsonStrategy.php',
);

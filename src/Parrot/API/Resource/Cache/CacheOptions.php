<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Cache;

/**
 * Class CacheOptions
 * @package Parrot\API\Resource\Cache
 */
class CacheOptions
{
    const PUBLIC_ACCESS  = 'public';
    const PRIVATE_ACCESS = 'private';
    const NOCACHE        = 'no-cache';

    /**
     * Value for when a Resource should be considered not fresh
     *
     * @var int
     */
    protected $maxAge = 3600; // 1 hour

    /**
     * Resource Cache expiry value
     *
     * @var int
     */
    protected $expires = 3600; // 1 hour

    /**
     * Determines if Resource should not be cached
     *
     * @var bool
     */
    protected $noCache = false;

    /**
     * Array of available access options
     *
     * @var array
     */
    protected $accessOptions = [self::PUBLIC_ACCESS, self::PRIVATE_ACCESS, self::NOCACHE];

    /**
     * @var string
     */
    protected $access = self::PRIVATE_ACCESS;

    /**
     * @param string $access
     * @return $this
     */
    public function setAccess($access)
    {
        if(in_array($access, $this->accessOptions))
        {
            $this->access = $access;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * @return array
     */
    public function getAccessOptions()
    {
        return $this->accessOptions;
    }

    /**
     * @param int $expires
     * @return $this
     */
    public function setExpires($expires)
    {
        $this->expires = (int) $expires;
        return $this;
    }

    /**
     * @return int
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @param int $maxAge
     * @return $this
     */
    public function setMaxAge($maxAge)
    {
        $this->maxAge = (int) $maxAge;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxAge()
    {
        return $this->maxAge;
    }

    /**
     * @param boolean $noCache
     * @return $this
     */
    public function setNoCache($noCache)
    {
        $this->noCache = (bool) $noCache;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getNoCache()
    {
        return $this->noCache;
    }
} 
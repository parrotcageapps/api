<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Renderer\Plugin;

/**
 * Class PluginAwareTrait
 * @package Parrot\API\Resource\Renderer\Plugin
 */
trait PluginAwareTrait
{
    /**
     * @var PluginInterface
     */
    protected $plugin;

    /**
     * Set Plugin used to Render Resource/Collections
     *
     * @param \Parrot\API\Resource\Renderer\Plugin\PluginInterface $plugin
     */
    public function setPlugin(PluginInterface $plugin)
    {
        $this->plugin = $plugin;
    }

    /**
     * Get Plugin to use for Rendering Resource/Collections
     *
     * @return \Parrot\API\Resource\Renderer\Plugin\PluginInterface
     */
    public function getPlugin()
    {
        return $this->plugin;
    }
} 
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Renderer\Plugin;

use Parrot\API\Resource\Backend\Collection\Collection;
use Parrot\API\Resource\Backend\ResourceInterface;

/**
 * Interface PluginInterface
 * @package Parrot\API\Resource\Renderer\Plugin
 */
interface PluginInterface
{
    /**
     * Render a Backend Resource
     *
     * @param \Parrot\API\Resource\Backend\Resource|ResourceInterface $resource
     * @return mixed
     */
    public function renderResource(ResourceInterface $resource);

    /**
     * Render a Backend Collection
     *
     * @param Collection $collection
     * @return mixed
     */
    public function renderCollection(Collection $collection);
} 
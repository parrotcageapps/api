<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Renderer\Plugin\JMS;

use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Parrot\API\Resource\Backend\Collection\Collection;
use Parrot\API\Resource\Backend\ResourceInterface;
use Parrot\API\Resource\Renderer\Plugin\PluginInterface;

/**
 * Class JsonPlugin
 * @package Parrot\API\Resource\Renderer\Plugin\JMS
 */
class JsonPlugin implements PluginInterface
{
    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * Constructor
     *
     * By Default if a JMS Serializer isn't provided, one will be created.
     * Providing a JMS Serializer instance allows for additional event handling,
     * caching and extensions to be loaded
     *
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer = null)
    {
        if(is_null($serializer))
        {
            $serializer = SerializerBuilder::create()->build();
        }

        $this->serializer = $serializer;
    }

    /**
     * Render a Backend Resource
     *
     * @param \Parrot\API\Resource\Backend\Resource|ResourceInterface $resource
     * @return mixed
     */
    public function renderResource(ResourceInterface $resource)
    {
        return $this->serializer->serialize($resource, 'json');
    }

    /**
     * Render a Backend Collection
     *
     * @param Collection $collection
     * @return mixed
     */
    public function renderCollection(Collection $collection)
    {
        return $this->serializer->serialize($collection, 'json');
    }
}
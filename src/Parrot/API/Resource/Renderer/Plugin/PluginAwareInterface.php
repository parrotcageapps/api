<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Renderer\Plugin;

/**
 * Interface PluginAwareInterface
 * @package Parrot\API\Resource\Renderer\Plugin
 */
interface PluginAwareInterface
{
    /**
     * Set Plugin used to Render Resource/Collections
     *
     * @param PluginInterface $plugin
     */
    public function setPlugin(PluginInterface $plugin);

    /**
     * Get Plugin to use for Rendering Resource/Collections
     *
     * @return PluginInterface
     */
    public function getPlugin();
} 
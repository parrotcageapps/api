<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Controller;

use Parrot\API\Problem\Exception\DomainException;
use Parrot\API\Problem\Problem;
use Parrot\API\Problem\Response\ProblemResponse;
use Parrot\API\Resource\Backend\Collection\Collection as BackendCollection;
use Parrot\API\Resource\Exception\ResourceNotImplementedException;
use Parrot\API\Resource\Backend\ResourceInterface as BackendResource;
use Parrot\API\Resource\Resource;
use Parrot\API\Resource\Service\ResourceLocatorAwareInterface;
use Parrot\API\Resource\Service\ResourceLocatorAwareTrait;
use Parrot\API\Resource\View\Model\ResourceJsonModel;
use Zend\Http\Header\Allow;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\MvcEvent;

/**
 * Class ResourceController
 * @package Parrot\API\Resource\Controller
 */
class ResourceController extends AbstractRestfulController implements ResourceLocatorAwareInterface
{
    use ResourceLocatorAwareTrait;

    /**
     * Resource associated with this Controller
     *
     * @var Resource
     */
    protected $resource;

    /**
     * Handle dispatch event
     *
     * Does several "pre-flight" checks:
     * - Raises an exception if no resource is composed.
     * - Raises an exception if no route is composed.
     * - Returns a 405 response if the current HTTP request method is not in
     *   $options
     *
     * @param MvcEvent $e
     * @return mixed|ProblemResponse
     * @throws \Parrot\API\Resource\Exception\ResourceNotImplementedException
     */
    public function onDispatch(MvcEvent $e)
    {
        if (!$this->getResource() && !$this->locateResource($e->getRouteMatch()->getParam('resource'))) {
            throw new ResourceNotImplementedException();
        }

        // Check for an API-Problem in the event
        $return = $e->getParam('api-problem', false);

        // If no API-Problem, dispatch the parent event
        if (!$return) {
            $return = parent::onDispatch($e);
        }

        if (!$return instanceof Problem) {
            return $return;
        }

        if ($return instanceof Problem) {
            return new ProblemResponse($return);
        }
    }

    /**
     * Create a new resource
     *
     * @param  mixed $data
     * @return Response|Problem|ProblemResponse
     */
    public function create($data)
    {
        $results  = $this->getEventManager()->trigger(__FUNCTION__.'.pre', $this, array('data' => $data));
        if(is_array($results->last()))
        {
            $data = $results->last();
        }

        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse)
        {
            return $results->last();
        }

        $resource = $this->getResource()->create($data);

        if($resource instanceof Problem
            || $resource instanceof ProblemResponse)
        {
            return $resource;
        }else{
            // Prepare a Response
            $resource = $this->prepareResource($resource);
        }

        $results = $this->getEventManager()->trigger(__FUNCTION__.'.post', $this, array('data' => $data, 'resource' => $resource));
        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse
            || $results->last() instanceof Response)
        {
            return $results->last();
        }

        $response = $this->getResponse();
        $response->setStatusCode(201);

        return $response;
    }

    /**
     * Delete an existing resource
     *
     * @param  mixed $id
     * @return Response|Problem|ProblemResponse
     */
    public function delete($id)
    {
        $this->getEventManager()->trigger(__FUNCTION__.'.pre', $this, array('id' => $id));

        $result = $this->getResource()->delete($id);

        if($result instanceof Problem
            || $result instanceof ProblemResponse)
        {
            return $result;
        }

        $results = $this->getEventManager()->trigger(__FUNCTION__.'.post', $this, array('id' => $id));
        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse)
        {
            return $results->last();
        }

        $response = $this->getResponse();
        $response->setStatusCode(204);

        return $response;
    }

    /**
     * Delete an existing collection of resources
     *
     * @param  null|array $data
     * @return bool
     */
    public function deleteList($data = null)
    {
        $results  = $this->getEventManager()->trigger(__FUNCTION__.'.pre', $this, array('data' => $data));
        if(is_array($results->last()))
        {
            $data = $results->last();
        }

        $result = $this->getResource()->deleteList($data);

        if($result instanceof Problem
            || $result instanceof ProblemResponse)
        {
            return $result;
        }

        $results = $this->getEventManager()->trigger(__FUNCTION__.'.post', $this, array());
        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse)
        {
            return $results->last();
        }

        $response = $this->getResponse();
        $response->setStatusCode(204);

        return $response;
    }

    /**
     * Return single resource
     *
     * @param  mixed $id
     * @return Resource|Problem|ProblemResponse
     */
    public function get($id)
    {
        $results  = $this->getEventManager()->triggerUntil(__FUNCTION__.'.pre', $this, array('id' => $id), function ($result){
            return $result instanceof Problem || $result instanceof ProblemResponse;
        });

        if($results->stopped() && ($results->last() instanceof Problem
                || $results->last() instanceof ProblemResponse))
        {
            return $results->last();
        }

        $resource = $this->getResource()->fetch($id);

        if(!$resource instanceof Problem
            && !$resource instanceof ProblemResponse)
        {
            // Prepare a Response
            $resource = $this->prepareResource($resource);
        }

        $results = $this->getEventManager()->trigger(__FUNCTION__.'.post', $this, array('id' => $id, 'resource' => $resource));
        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse)
        {
            return $results->last();
        }

        /**
         * Check last returned result for either a Resource or a valid Response
         * Otherwise return the pre .post event Resource/Response
         */
        return ($results->last() instanceof BackendResource
            || $results->last() instanceof Response) ? $results->last() : $resource;
    }

    /**
     * Return list of resources
     *
     * @param array $data
     * @return Collection|Problem|ProblemResponse
     */
    public function getList($data = array())
    {
        $results  = $this->getEventManager()->triggerUntil(__FUNCTION__.'.pre', $this, array('data' => $data), function ($result){
            return $result instanceof Problem || $result instanceof ProblemResponse;
        });

        if($results->stopped() && ($results->last() instanceof Problem
                || $results->last() instanceof ProblemResponse))
        {
            return $results->last();
        }

        if(is_array($results->last()))
        {
            $data = $results->last();
        }

        $collection = $this->getResource()->fetchAll($data);

        if(!$collection instanceof Problem
            && !$collection instanceof ProblemResponse)
        {
            $collection = $this->prepareCollection($collection);
        }

        $results = $this->getEventManager()->trigger(__FUNCTION__.'.post', $this, array('collection' => $collection));
        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse)
        {
            return $results->last();
        }

        /**
         * Check last returned result for either a Collection or a valid Response
         * Otherwise return the pre .post event Collection/Response
         */
        return ($results->last() instanceof BackendCollection
            || $results->last() instanceof Response) ? $results->last() : $collection;
    }

    /**
     * Update an existing resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return Resource|Problem|ProblemResponse
     */
    public function update($id, $data)
    {
        $results  = $this->getEventManager()->trigger(__FUNCTION__.'.pre', $this, array('id' => $id, 'data' => $data));
        if(is_array($results->last()))
        {
            $data = $results->last();
        }

        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse)
        {
            return $results->last();
        }

        $resource = $this->getResource()->update($id, $data);

        if($resource instanceof Problem
            || $resource instanceof ProblemResponse)
        {
            return $resource;
        }else{
            // Prepare a Response
            $resource = $this->prepareResource($resource);
        }

        $results = $this->getEventManager()->trigger(__FUNCTION__.'.post', $this, array('id' => $id, 'data' => $data, 'resource' => $resource));
        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse
            || $results->last() instanceof Response)
        {
            return $results->last();
        }

        $response = $this->getResponse();
        $response->setStatusCode(204);

        return $response;
    }

    /**
     * Update (replace) an existing collection of resources
     *
     * @param  array $data
     * @return Collection|Problem|ProblemResponse
     */
    public function replaceList($data)
    {
        $results  = $this->getEventManager()->trigger(__FUNCTION__.'.pre', $this, array('data' => $data));
        if(is_array($results->last()))
        {
            $data = $results->last();
        }

        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse)
        {
            return $results->last();
        }

        $collection = $this->getResource()->replaceList($data);

        if($collection instanceof Problem
            || $collection instanceof ProblemResponse)
        {
            return $collection;
        }else{
            // Prepare a Response
            $collection = $this->prepareCollection($collection);
        }

        $results = $this->getEventManager()->trigger(__FUNCTION__.'.post', $this, array('data' => $data, 'collection' => $collection));
        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse)
        {
            return $results->last();
        }

        $response = $this->getResponse();
        $response->setStatusCode(204);

        return $response;
    }

    /**
     * Partial update of an existing resource
     *
     * @param  string|int $id
     * @param  array|object $data
     * @return Resource|Problem|ProblemResponse
     */
    public function patch($id, $data)
    {
        $results  = $this->getEventManager()->trigger(__FUNCTION__.'.pre', $this, array('id' => $id, 'data' => $data));
        if(is_array($results->last()))
        {
            $data = $results->last();
        }

        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse)
        {
            return $results->last();
        }

        $resource = $this->getResource()->patch($id, $data);

        if($resource instanceof Problem
            || $resource instanceof ProblemResponse)
        {
            return $resource;
        }else{
            // Prepare a Response
            $resource = $this->prepareResource($resource);
        }

        $results = $this->getEventManager()->trigger(__FUNCTION__.'.post', $this, array('id' => $id, 'data' => $data, 'resource' => $resource));
        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse
            || $results->last() instanceof Response)
        {
            return $results->last();
        }

        $response = $this->getResponse();
        $response->setStatusCode(204);

        return $response;
    }

    /**
     * Partial update of an existing collection
     *
     * @param  array|object $data
     * @return Collection|Problem|ProblemResponse
     */
    public function patchList($data)
    {
        $results  = $this->getEventManager()->trigger(__FUNCTION__.'.pre', $this, array('data' => $data));
        if(is_array($results->last()))
        {
            $data = $results->last();
        }

        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse)
        {
            return $results->last();
        }

        $collection = $this->getResource()->patchList($data);

        if($collection instanceof Problem
            || $collection instanceof ProblemResponse)
        {
            return $collection;
        }else{
            // Prepare a Response
            $collection = $this->prepareCollection($collection);
        }

        $results = $this->getEventManager()->trigger(__FUNCTION__.'.post', $this, array('data' => $data, 'collection' => $collection));
        if($results->last() instanceof Problem
            || $results->last() instanceof ProblemResponse)
        {
            return $results->last();
        }

        $response = $this->getResponse();
        $response->setStatusCode(204);

        return $response;
    }

    /**
     * Retrieve HEAD metadata for the resource and or collection
     *
     * @param null $id
     * @return mixed
     */
    public function head($id = null)
    {
        if($id)
        {
            return $this->get($id);
        }

        return $this->getList();
    }

    /**
     * Respond to OPTIONS request
     *
     * Uses $options to set the Allow header line and return an empty response.
     *
     * @return Response
     */
    public function options()
    {
        if (null === $id = $this->params()->fromRoute('id')) {
            $id = $this->params()->fromQuery('id');
        }

        $response = $this->getResponse();
        $response->setStatusCode(204);
        $headers = $response->getHeaders();

        $allow = new Allow();
        $options = $this->getResource()->options();
        $allow->allowMethods($options);
        $headers->addHeader($allow);

        return $response;
    }

    /**
     * Set Resource
     *
     * @param \Parrot\API\Resource\Resource $resource
     */
    public function setResource(Resource $resource)
    {
        $this->resource = $resource;

        /**
         * Set Resource Event Manager keeping events
         * in the scope of this controller only.
         */
        $this->resource->setEventManager($this->getEventManager());
    }

    /**
     * Get Resource
     *
     * @return \Parrot\API\Resource\Resource Resource
     * @throws DomainException
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Locates a Resource by it's identifier
     *
     * @param $identifier
     * @return bool|Resource
     */
    public function locateResource($identifier)
    {
        $resource = call_user_func($this->getResourceLocatorService(), $identifier);
        if($resource)
        {
            $this->setResource($resource);
        }

        return $resource;
    }



    /**
     * Prepares an Resource Json Model from Resource
     *
     * @param BackendResource $resource
     * @return ResourceJsonModel
     */
    protected function prepareResource(BackendResource $resource)
    {
        $model = new ResourceJsonModel();
        $model->setPayload($resource);

        return $model;
    }

    /**
     * Prepares an Resource Json Model from Collection
     *
     * @param BackendCollection $collection
     * @return ResourceJsonModel
     */
    protected function prepareCollection(BackendCollection $collection)
    {
        $model = new ResourceJsonModel();
        $model->setPayload($collection);

        return $model;
    }

    /**
     * Creates a "405 Method Not Allowed" response detailing the available methods
     *
     * @param  array $methods
     * @return Response
     */
    protected function createMethodNotAllowedResponse(array $methods)
    {
        $response = $this->getResponse();
        $response->setStatusCode(405);
        $headers = $response->getHeaders();
        $headers->addHeader($this->createAllowHeaderWithAllowedMethods($methods));

        return $response;
    }

    /**
     * Creates an ALLOW header with the provided HTTP methods
     *
     * @param  array $methods
     * @return Allow
     */
    protected function createAllowHeaderWithAllowedMethods(array $methods)
    {
        // Need to create an Allow header. It has several defaults, and the only
        // way to start with a clean slate is to retrieve all methods, disallow
        // them all, and then set the ones we want to allow.
        $allow      = new Allow();
        $allMethods = $allow->getAllMethods();
        $allow->disallowMethods(array_keys($allMethods));
        $allow->allowMethods($methods);

        return $allow;
    }
}
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Factory;

use Parrot\API\Resource\Resource;
use Parrot\API\Resource\InputFilter\Resource as ResourceInputFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ResourceFactory
 * @package Parrot\API\Resource\Factory
 *
 * Creates the default API Resource
 */
class ResourceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $resource = new Resource('default');

        // Set Dependencies
        $resource->setUrlHelper($serviceLocator->get('ViewHelperManager')->get('Url'));
        $resource->setServerUrlHelper($serviceLocator->get('ViewHelperManager')->get('ServerUrl'));

        // Set Input Filter
        $resourceInputFilter = new ResourceInputFilter($serviceLocator->get('HTMLPurifier'));
        $resource->setInputFilter($resourceInputFilter);

        $resource->setCacheAdapter($serviceLocator->get('Parrot\API\Resource\Cache'));

        return $resource;
    }
}
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Factory;

use Parrot\API\Resource\Renderer\Plugin\JMS\JsonPlugin;
use Parrot\API\Resource\View\Renderer\ResourceJsonRenderer;
use Parrot\API\Resource\View\Strategy\ResourceJsonHalStrategy;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ResourceJsonHalStrategyFactory
 * @package Parrot\API\Resource\Factory
 */
class ResourceJsonHalStrategyFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $resourceJsonRenderer = new ResourceJsonRenderer();
        $resourceJsonRenderer->setPlugin(new JsonPlugin());

        return new ResourceJsonHalStrategy($resourceJsonRenderer);
    }
}
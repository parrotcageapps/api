<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Factory;

use Parrot\API\Resource\Listener\ResponseCacheListenerAggregate;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ResponseCacheListenerFactory
 * @package Parrot\API\Resource\Factory
 */
class ResponseCacheListenerFactory implements FactoryInterface
{
    /**
     * Create Service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed|void
     * @throws \Zend\ServiceManager\Exception\ServiceNotCreatedException
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $responseCacheListener = new ResponseCacheListenerAggregate($serviceLocator->get('Parrot\API\Resource\Cache'));
        $responseCacheListener->setResourceLocatorService($serviceLocator->get('Parrot\API\Service\ResourceLocator'));

        return $responseCacheListener;
    }
} 
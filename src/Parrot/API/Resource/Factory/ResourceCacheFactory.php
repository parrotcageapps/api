<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Factory;

use Zend\Cache\Storage\Adapter\AbstractAdapter;
use Zend\Cache\Storage\Plugin\AbstractPlugin;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ResourceCacheFactory
 * @package Parrot\API\Resource\Factory
 */
class ResourceCacheFactory implements FactoryInterface
{
    /**
     * Create Service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed|void
     * @throws \Zend\ServiceManager\Exception\ServiceNotCreatedException
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        if(isset($config['parrot_api'])
            && isset($config['parrot_api']['resource_cache']))
        {
            $config = $config['parrot_api']['resource_cache'];

            // Retrieve Cache Adapter
            if($serviceLocator->has($config['adapter']))
            {
                $cacheAdapter = $serviceLocator->get($config['adapter']);
            }else{
                $cacheAdapterClassName = $config['adapter'];
                if(class_exists($cacheAdapterClassName))
                {
                    $cacheAdapter = new $cacheAdapterClassName;
                }
            }

            if($cacheAdapter instanceof AbstractAdapter)
            {
                if(isset($config['adapter']['options']))
                {
                    $cacheAdapter->setOptions($config['adapter']['options']);
                }

                if(isset($config['adapter']['plugins']) && is_array($config['adapter']['plugins']))
                {
                    foreach($config['adapter']['plugins'] as $pluginclassName)
                    {
                        if(class_exists($pluginclassName))
                        {
                            $plugin = new $pluginclassName;
                            if($plugin instanceof AbstractPlugin)
                            {
                                $cacheAdapter->addPlugin($plugin);
                            }
                        }
                    }
                }
            }

            return $cacheAdapter;
        }

        throw new ServiceNotCreatedException();
    }
}
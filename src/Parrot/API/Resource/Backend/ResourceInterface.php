<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend;

/**
 * Interface ResourceInterface
 * @package Parrot\API\Resource\Backend
 */
interface ResourceInterface
{
    /**
     * Get Resource id
     *
     * @return int
     */
    public function getId();

    /**
     * Resource Identifier
     *
     * @return string
     */
    public function getIdentifier();
}
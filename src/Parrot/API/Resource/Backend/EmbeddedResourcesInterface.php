<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend;

use JMS\Serializer\Annotation as JMS;

/**
 * Interface EmbeddedResourcesInterface
 * @package Parrot\API\Resource\Backend
 *
 * Identifies a Resource which has embedded Resources
 */
interface EmbeddedResourcesInterface
{
    /**
     * Get Embedded Resources
     *
     * @return mixed
     */
    public function getEmbeddedResources();

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("_embedded")
     *
     * @return mixed
     */
    public function exposeEmbeddedResources();
}
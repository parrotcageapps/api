<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend\Doctrine;

use Parrot\Database\Doctrine\Service\Entity as EntityService;
use Parrot\Database\Doctrine\Service\EntityServiceAwareInterface;
use Parrot\Database\Doctrine\Service\EntityServiceAwareTrait;

/**
 * Class EntityBackend
 * @package Parrot\API\Resource\Backend\Doctrine
 */
class EntityBackend extends AbstractBackend implements EntityServiceAwareInterface
{
    use EntityServiceAwareTrait;

    /**
     * Database Service
     *
     * @return EntityService
     */
    protected function getObjectService()
    {
        return $this->getEntityService();
    }
}
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend\Doctrine;

use Doctrine\ORM\ORMException;
use Parrot\API\Problem\Problem;
use Parrot\API\Problem\Response\ProblemResponse;
use Parrot\API\Resource\Backend\Collection\Collection;
use Parrot\API\Resource\Backend\ResourceInterface;
use Parrot\API\Resource\Pluralization;
use Parrot\API\Resource\Backend\BaseBackend;
use Parrot\Database\DatabaseObjectInterface;
use Parrot\Database\Doctrine\Service\AbstractService;
use Parrot\Database\Doctrine\Service\DocumentServiceAwareInterface;
use Parrot\Database\Doctrine\Service\DocumentServiceAwareTrait;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Stdlib\Parameters;

/**
 * Class AbstractBackend
 * @package Parrot\API\Resource\Backend\Doctrine
 */
abstract class AbstractBackend extends BaseBackend
{
    /**
     * @var string
     */
    protected $objectClass;

    /**
     * @var DatabaseObjectInterface
     */
    protected $object;

    /**
     * Constructor
     *
     * @param string
     */
    public function __construct($objectClass)
    {
        $this->objectClass = $objectClass;
    }

    /**
     * Create a record in the resource
     *
     * @param  array|object $data
     * @return ResourceInterface|Problem|ProblemResponse
     */
    public function create($data)
    {
        $object = $this->getObject();
        $object = $this->hydrateObject($data, $object);

        try{
            $object = $this->getObjectService()->insert($object);

            if($object instanceof DatabaseObjectInterface)
            {
                if($object instanceof ResourceInterface)
                {
                    return $object;
                }
            }

        }catch (ORMException $e) {
            return new Problem($e->getCode(), $e->getMessage());
        }

        return new Problem(409, 'Unable to create Resource');
    }

    /**
     * Fetch a Single Resource
     *
     * @param int|string $id
     * @return ResourceInterface|Problem|ProblemResponse
     */
    public function fetch($id)
    {
        // Use Object Service to find an Object by the provided id
        if($object = $this->getObjectService()->find($this->getObjectClass() ,$id))
        {
            if($object instanceof DatabaseObjectInterface)
            {
                return $object;
            }
        }

        return new Problem(404, 'Resource not found');
    }

    /**
     * Fetch a Collection of Resources
     *
     * @param $data
     * @return Collection|Problem|ProblemResponse
     */
    public function fetchAll($data = array())
    {
        if($data instanceof Parameters)
        {
            $data = $data->toArray();
        }

        $page     = isset($data['page'])? ($data['page']-1) : 0;
        $offset   = $this->getPaginationLimit() * $page;
        $criteria = isset($data['criteria'])? $data['criteria'] : array();
        $orderBy  = isset($data['order_by'])? $data['order_by'] : array();

        $objects = $this->getObjectService()
            ->findBy($this->getObjectClass(), $criteria, $orderBy, $this->getPaginationLimit(), $offset);

        // Check if objects returned are encapsulated within an object instead of an array
        if(is_object($objects) && method_exists($objects, 'toArray'))
        {
            $objects = $objects->toArray();
        }

        if(sizeof($objects) > 0)
        {
            $collection = new Collection($objects, Pluralization::pluralize($this->getEvent()->getResourceIdentifier()));

            $count = $this->getObjectService()->count($this->getObjectClass());
            $collection->setPagination($this->preparePagination($page, $this->getPaginationLimit(), $count));

            return $collection;
        }

        return new Problem(404, 'Collection not found');
    }

    /**
     * Update (replace) an existing Resource
     *
     * @param  string|int $id
     * @param  array|object $data
     * @return ResourceInterface|Problem|ProblemResponse
     */
    public function update($id, $data)
    {
        if($data instanceof Parameters)
        {
            $data = $data->toArray();
        }

        $object = $this->getObjectService()->find($this->getObjectClass(), $id);
        if($object instanceof DatabaseObjectInterface)
        {
            $object = $this->hydrateObject($data, $object);

            try{
                $object = $this->getObjectService()->update($object);

                if($object instanceof DatabaseObjectInterface)
                {
                    if($object instanceof ResourceInterface)
                    {
                        return $object;
                    }
                }

            }catch (ORMException $e) {
                return new Problem($e->getCode(), $e->getMessage());
            }
        }

        return new Problem(409, 'Unable to update Resource');
    }

    /**
     * Partial update of an existing Resource
     *
     * @param  string|int $id
     * @param  array|object $data
     * @return ResourceInterface|Problem|ProblemResponse
     */
    public function patch($id, $data)
    {
        /**
         * Semantically patch should allow partial updates,
         * but in reality it's functionality is the same as update
         */
        return $this->update($id, $data);
    }

    /**
     * Delete an existing resource
     *
     * @param  string|int $id
     * @return bool|Problem
     */
    public function delete($id)
    {
        $object = $this->getObjectService()->find($this->getObjectClass(), $id);

        if($object instanceof DatabaseObjectInterface)
        {
            if($this->getObjectService()->delete($object))
            {
                return true;
            }
        }

        return new Problem(422, 'Unable to delete Resource');
    }

    /**
     * Set Object Class
     *
     * @param string
     */
    public function setObjectClass($objectClass)
    {
        $this->objectClass = $objectClass;
    }

    /**
     * Get Object
     *
     * @return string
     */
    public function getObjectClass()
    {
        return $this->objectClass;
    }

    /**
     * Set Object
     *
     * @param DatabaseObjectInterface $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * Get Object
     *
     * @return DatabaseObjectInterface
     */
    public function getObject()
    {
        if(!$this->object instanceof DatabaseObjectInterface)
        {
            $objectClass = $this->getObjectClass();
            $this->setObject(new $objectClass);
        }

        return $this->object;
    }

    /**
     * Hydrates a Database Object using provided data
     *
     * @param $data
     * @param DatabaseObjectInterface $object
     * @return DatabaseObjectInterface
     */
    protected function hydrateObject($data, DatabaseObjectInterface $object)
    {
        $classMethodHydrator = new ClassMethods();
        $classMethodHydrator->setUnderscoreSeparatedKeys(true);

        $object = $classMethodHydrator->hydrate($data, $object);

        return $object;
    }

    /**
     * Database Service
     *
     * @return AbstractService
     */
    abstract protected function getObjectService();
} 
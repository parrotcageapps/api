<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend\Doctrine;

use Parrot\Database\Doctrine\Service\Document as DocumentService;
use Parrot\Database\Doctrine\Service\DocumentServiceAwareInterface;
use Parrot\Database\Doctrine\Service\DocumentServiceAwareTrait;


/**
 * Class DocumentBackend
 * @package Parrot\API\Resource\Backend\Doctrine
 */
class DocumentBackend extends AbstractBackend
{
    use DocumentServiceAwareTrait;

    /**
     * Database Service
     *
     * @return DocumentService
     */
    protected function getObjectService()
    {
        return $this->getDocumentService();
    }
}
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend\Guzzle;

/**
 * Class RequestMap
 * @package Parrot\API\Resource\Backend\Guzzle
 */
class RequestMap
{
    protected $getBase;
    protected $postBase;
    protected $putBase;
} 
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend\Guzzle;

use Parrot\API\Resource\Backend\BaseBackend;

/**
 * Class GuzzleBackend
 * @package Parrot\API\Resource\Backend\Guzzle
 *
 * Used to make API requests on behalf of a Resource
 * There maybe times where a private API has to be utilised behind a public service
 * Guzzle Backend can be used to marshal those requests and provide a tailored response
 */
class GuzzleBackend extends BaseBackend implements GuzzleClientAwareInterface
{
    use GuzzleClientAwareTrait;
} 
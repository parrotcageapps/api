<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend\Guzzle;

use Guzzle\Http\Client;

/**
 * Interface GuzzleClientAwareInterface
 * @package Parrot\API\Resource\Backend\Guzzle
 */
interface GuzzleClientAwareInterface
{
    /**
     * Set Guzzle HTTP Client
     *
     * @param Client $client
     */
    public function setGuzzleClient(Client $client);

    /**
     * Get Guzzle HTTP Client
     *
     * @return Client
     */
    public function getGuzzleClient();
} 
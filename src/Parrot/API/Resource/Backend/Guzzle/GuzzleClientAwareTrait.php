<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend\Guzzle;

use Guzzle\Http\Client;

/**
 * Class GuzzleClientAwareTrait
 * @package Parrot\API\Resource\Backend\Guzzle
 */
trait GuzzleClientAwareTrait
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * Set Guzzle HTTP Client
     *
     * @param Client $client
     */
    public function setGuzzleClient(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Get Guzzle HTTP Client
     *
     * @return Client
     */
    public function getGuzzleClient()
    {
        return $this->client;
    }
} 
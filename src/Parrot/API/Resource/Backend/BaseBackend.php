<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend;

use Parrot\API\Problem\Exception\DomainException;
use Parrot\API\Problem\Problem;
use Parrot\API\Problem\Response\ProblemResponse;
use Parrot\API\Resource\Backend\Collection\Collection;
use Parrot\API\Resource\Backend\Collection\Pagination;
use Parrot\API\Resource\ResourceEvent;
use Parrot\API\Resource\Backend\ResourceInterface as BackendResourceInterface;
use Parrot\API\Resource\ResourceInterface as ApiResourceInterface;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class BaseBackend
 * @package Parrot\API\Resource\Backend
 */
abstract class BaseBackend extends AbstractListenerAggregate implements ApiResourceInterface
{
    /**
     * @var ResourceEvent
     */
    protected $event;

    /**
     * Backend Priority
     *
     * Higher the priority the more important the backend is to check prior,
     * to others registered with a Resource
     *
     * @var int
     */
    protected $priority = 1;

    /**
     * Number of resources to return per page
     *
     * @var int
     */
    protected $paginationLimit = 10;

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $events->attach('create',      array($this, 'dispatch'), $this->getPriority());
        $events->attach('delete',      array($this, 'dispatch'), $this->getPriority());
        $events->attach('deleteList',  array($this, 'dispatch'), $this->getPriority());
        $events->attach('fetch',       array($this, 'dispatch'), $this->getPriority());
        $events->attach('fetchAll',    array($this, 'dispatch'), $this->getPriority());
        $events->attach('patch',       array($this, 'dispatch'), $this->getPriority());
        $events->attach('patchList',   array($this, 'dispatch'), $this->getPriority());
        $events->attach('replaceList', array($this, 'dispatch'), $this->getPriority());
        $events->attach('update',      array($this, 'dispatch'), $this->getPriority());
    }

    /**
     * Dispatch an incoming event to the appropriate method
     *
     * Marshals arguments from the event parameters.
     * @param ResourceEvent $event
     * @return array|bool|false|object|\Zend\Paginator\Paginator
     * @throws \Parrot\API\Problem\Exception\DomainException
     */
    public function dispatch(ResourceEvent $event)
    {
        $this->setEvent($event);

        switch ($event->getName()) {
            case 'create':
                $data = $event->getParam('data', array());
                return $this->create($data);
            case 'delete':
                $id   = $event->getParam('id', null);
                return $this->delete($id);
            case 'deleteList':
                $data = $event->getParam('data', array());
                return $this->deleteList($data);
            case 'fetch':
                $id   = $event->getParam('id', null);
                return $this->fetch($id);
            case 'fetchAll':
                /**
                 * Merge Query params used for pagination with Params
                 * passed to the Event from listeners modifying
                 * query params or providing additional criteria
                 */
                $params = ($event->getParams() instanceof \ArrayObject) ? $event->getParams()->getArrayCopy() : $event->getParams();
                $params = ArrayUtils::merge($event->getQueryParams()->toArray(), $params['data']);
                return $this->fetchAll($params);
            case 'patch':
                $id   = $event->getParam('id', null);
                $data = $event->getParam('data', array());
                return $this->patch($id, $data);
            case 'patchList':
                $data = $event->getParam('data', array());
                return $this->patchList($data);
            case 'replaceList':
                $data = $event->getParam('data', array());
                return $this->replaceList($data);
            case 'update':
                $id   = $event->getParam('id', null);
                $data = $event->getParam('data', array());
                return $this->update($id, $data);
            default:
                throw new DomainException(sprintf(
                    '%s has not been setup to handle the event "%s"',
                    __METHOD__,
                    $event->getName()
                ));
        }
    }

    /**
     * Create a record in the Resource
     *
     * @param  array|object $data
     * @return BackendResourceInterface|Problem|ProblemResponse
     */
    public function create($data)
    {
        return new Problem(405, 'The POST method has not been defined for this resource');
    }

    /**
     * Update (replace) an existing Resource
     *
     * @param  string|int $id
     * @param  array|object $data
     * @return BackendResourceInterface|Problem|ProblemResponse
     */
    public function update($id, $data)
    {
        return new Problem(405, 'The PUT method has not been defined for this resource');
    }

    /**
     * Update (replace) an existing Collection of resources
     *
     * @param  array $data
     * @return Collection|Problem|ProblemResponse
     */
    public function replaceList($data)
    {
        return new Problem(405, 'The PUT method has not been defined for this resource');
    }

    /**
     * Partial update of an existing Resource
     *
     * @param  string|int $id
     * @param  array|object $data
     * @return BackendResourceInterface|Problem|ProblemResponse
     */
    public function patch($id, $data)
    {
        return new Problem(405, 'The PATCH method has not been defined for this resource');
    }

    /**
     * Partial update of an existing collection
     *
     * @param  array|object $data
     * @return Collection|Problem|ProblemResponse
     */
    public function patchList($data)
    {
        return new Problem(405, 'The PATCH method has not been defined for this resource');
    }

    /**
     * Delete an existing Resource
     *
     * @param  string|int $id
     * @return bool|Problem|ProblemResponse
     */
    public function delete($id)
    {
        return new Problem(405, 'The DELETE method has not been defined for this resource');
    }

    /**
     * Delete an existing Collection of resources
     *
     * @param  null|array $data
     * @return bool|Problem|ProblemResponse
     */
    public function deleteList($data = null)
    {
        return new Problem(405, 'The DELETE method has not been defined for this resource');
    }

    /**
     * Fetch an existing Resource
     *
     * @param  string|int $id
     * @return BackendResourceInterface|Problem|ProblemResponse
     */
    public function fetch($id)
    {
        return new Problem(405, 'The GET method has not been defined for this resource');
    }

    /**
     * Fetch a Collection of resources
     *
     * @param array $data
     * @return Collection|Problem|ProblemResponse
     */
    public function fetchAll($data = array())
    {
        return new Problem(405, 'The GET method has not been defined for this resource');
    }

    /**
     * Set number of Resources to return per page
     *
     * @param int $paginationLimit
     */
    public function setPaginationLimit($paginationLimit)
    {
        $this->paginationLimit = (int) $paginationLimit;
    }

    /**
     * Get number of Resources to return per page
     *
     * @return int
     */
    public function getPaginationLimit()
    {
        return $this->paginationLimit;
    }

    /**
     * Set Resource Event
     *
     * @param ResourceEvent $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * Get Resource Event
     *
     * @return ResourceEvent
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set Backend Priority
     *
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * Get Backend Priority
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Prepares Pagination for use within a Collection
     *
     * @param $page
     * @param $pageSize
     * @param $totalItems
     * @return Pagination
     */
    protected function preparePagination($page, $pageSize, $totalItems)
    {
        $pagination = new Pagination(
            $page,
            $pageSize,
            $totalItems
        );

        return $pagination;
    }
}
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend;

/**
 * Interface PassiveResourceInterface
 * @package Parrot\API\Resource\Backend
 *
 * Identifies a Passive Resource Backend
 * Usually for caching or augmenting an existing Backend Resource
 *
 * A passive Resource Backend should not return Problems or Problem Responses
 */
interface PassiveResourceInterface {}
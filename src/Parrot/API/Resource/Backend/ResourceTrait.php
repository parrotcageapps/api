<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend;

/**
 * Class ResourceTrait
 * @package Parrot\API\Resource\Backend
 */
trait ResourceTrait
{
    /**
     * Get Resource id
     *
     * @return int
     */
    abstract public function getId();

    /**
     * Resource Identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        $reflectionClass = new \ReflectionClass($this);
        $className = $reflectionClass->getShortName();

        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $className));
    }
} 
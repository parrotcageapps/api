<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend\Collection;

use Parrot\API\Problem\Exception\InvalidArgumentException;
use Parrot\API\Resource\Link\Link;
use Parrot\API\Resource\Link\LinkCollection;
use Parrot\API\Resource\Link\LinkCollectionAwareInterface;
use Parrot\API\Resource\ResourceEvent;
use Zend\Stdlib\ArrayUtils;

/**
 * Class Pagination
 * @package Parrot\API\Resource\Backend\Collection\Pagination
 */
class Pagination implements LinkCollectionAwareInterface
{
    /**
     * @var int
     */
    protected $page = 1;

    /**
     * @var int
     */
    protected $pageSize;

    /**
     * @var int
     */
    protected $totalItems;

    /**
     * @var LinkCollection
     */
    protected $linkCollection;

    /**
     * Construct Pagination
     *
     * @param $page
     * @param $pageSize
     * @param $totalItems
     */
    public function __construct($page, $pageSize, $totalItems)
    {
        $this->page           = $page;
        $this->pageSize       = $pageSize;
        $this->totalItems     = $totalItems;
        $this->linkCollection = new LinkCollection();
    }

    /**
     * Set current page
     *
     * @param $page
     * @return $this
     * @throws \Parrot\API\Problem\Exception\InvalidArgumentException
     */
    public function setPage($page)
    {
        if (!is_int($page) && !is_numeric($page)) {
            throw new InvalidArgumentException(sprintf(
                'Page must be an integer; received "%s"',
                gettype($page)
            ));
        }

        $page = (int) $page;
        if ($page < 1) {
            throw new InvalidArgumentException(sprintf(
                'Page must be a positive integer; received "%s"',
                $page
            ));
        }

        $this->page = $page;
        return $this;
    }

    /**
     * Get current page
     *
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set page size
     *
     * @param $pageSize
     * @return $this
     * @throws \Parrot\API\Problem\Exception\InvalidArgumentException
     */
    public function setPageSize($pageSize)
    {
        if (!is_int($pageSize) && !is_numeric($pageSize)) {
            throw new InvalidArgumentException(sprintf(
                'Page size must be an integer; received "%s"',
                gettype($pageSize)
            ));
        }

        $pageSize = (int) $pageSize;
        if ($pageSize < 1) {
            throw new InvalidArgumentException(sprintf(
                'size must be a positive integer; received "%s"',
                $pageSize
            ));
        }

        $this->pageSize = $pageSize;
        return $this;
    }

    /**
     * Get page size
     *
     * @return int
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * Set total items per page
     *
     * @param $totalItems
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setTotalItems($totalItems)
    {
        if (!is_int($totalItems) && !is_numeric($totalItems)) {
            throw new InvalidArgumentException(sprintf(
                'Total items must be an integer; received "%s"',
                gettype($totalItems)
            ));
        }

        $this->totalItems = $totalItems;
        return $this;
    }

    /**
     * Get total items per page
     *
     * @return int
     */
    public function getTotalItems()
    {
        return $this->totalItems;
    }

    /**
     * Set Link Collection
     *
     * @param LinkCollection $links
     */
    public function setLinks(LinkCollection $links)
    {
        $this->linkCollection = $links;
    }

    /**
     * Get Link Collection
     *
     * @return LinkCollection
     */
    public function getLinks()
    {
        return $this->linkCollection;
    }

    /**
     * Generates Links and add them to a Link Collection for pagination
     * using Link Relation Specification and a provided Resource Event
     *
     * @see http://www.iana.org/assignments/link-relations/link-relations.xhtml
     * @param ResourceEvent $e
     */
    public function composeLinksFromResourceEvent(ResourceEvent $e)
    {
        $params             = ($e->getParams() instanceof \ArrayObject) ? $e->getParams()->getArrayCopy() : $e->getParams();
        $params             = ArrayUtils::merge($e->getRouteMatch()->getParams(), $params);
        $params             = ArrayUtils::merge(array('resource' => $e->getResourceIdentifier()), $params);
        $route              = $e->getRouteMatch()->getMatchedRouteName();

        // Calculate last page
        $lastPage = round($this->getTotalItems()/$this->getPageSize());
        if($lastPage <= 1) {
            $lastPage = false;
        }

        $page = ($this->getPage() + 1);
        $nextPage  = ($page < $lastPage) ? $page + 1 : false;
        $prevPage  = ($page > 1)      ? $page - 1 : false;

        if($page > 1)
        {
            $first = new Link('first');
            $first->setRoute($route);
            $first->setRouteParams($params);
            $this->getLinks()->add($first);
        }

        if($nextPage)
        {
            $next = new Link('next');
            $next->setRoute($route);
            $next->setRouteParams($params);
            $next->setRouteOptions(
                array(
                    'query' => array(
                        'page' => $nextPage
                    )
                )
            );

            $this->getLinks()->add($next);
        }

        if($prevPage)
        {
            $prev = new Link('prev');
            $prev->setRoute($route);
            $prev->setRouteParams($params);
            $prev->setRouteOptions(
                array(
                    'query' => array(
                        'page' => $prevPage
                    )
                )
            );
            $this->getLinks()->add($prev);
        }

        if($lastPage)
        {
            $last = new Link('last');
            $last->setRoute($route);
            $last->setRouteParams($params);
            $last->setRouteOptions(
                array(
                    'query' => array(
                        'page' => $lastPage
                    )
                )
            );
            $this->getLinks()->add($last);
        }
    }
} 
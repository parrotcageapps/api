<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend\Collection;

use JMS\Serializer\Annotation as JMS;
use Parrot\API\Resource\Backend\EmbeddedResourcesInterface;
use Parrot\API\Resource\Backend\ResourceInterface;
use Parrot\API\Resource\Link\LinkCollection;
use Parrot\API\Resource\Link\LinkCollectionAwareInterface;
use Zend\Stdlib\ArrayUtils;

/**
 * Class Collection
 * @package Parrot\API\Resource\Backend\Collection
 *
 * @JMS\ExclusionPolicy("all")
 * @JMS\AccessorOrder("custom", custom = {"linkUrls", "exposeEmbeddedResources"})
 */
class Collection implements LinkCollectionAwareInterface, EmbeddedResourcesInterface
{
    /**
     * Collection Name
     *
     * @var string
     */
    protected $name;

    /**
     * @var ResourceInterface[]
     */
    protected $resources;

    /**
     * @var LinkCollection
     */
    protected $links;

    /**
     * @var Pagination
     */
    protected $pagination;

    /**
     * Constructor
     *
     * @param array $resources
     * @param string $name
     */
    public function __construct(array $resources = array(), $name = 'items')
    {
        $this->resources = $resources;
        $this->name = $name;
        $this->links = new LinkCollection();
    }

    /**
     * Add a Resource to the Collection
     *
     * @param ResourceInterface $resource
     */
    public function addResource(ResourceInterface $resource)
    {
        $this->resources[] = $resource;
    }

    /**
     * Set Resources
     *
     * @param array $resources
     */
    public function setResources(array $resources = array())
    {
        $this->resources = $resources;
    }

    /**
     *
     * Get Resources
     *
     * @return mixed
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * Get Embedded Resources
     *
     * @return mixed
     */
    public function getEmbeddedResources()
    {
        return $this->getResources();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("_embedded")
     *
     * @return mixed
     */
    public function exposeEmbeddedResources()
    {
        return array($this->getName() => $this->getResources());
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = (string) $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Link Collection
     *
     * @param LinkCollection $links
     */
    public function setLinks(LinkCollection $links)
    {
        $this->links = $links;
    }

    /**
     * Get Link Collection
     *
     * @return LinkCollection
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Set Collection Pagination
     *
     * @param Pagination $pagination
     */
    public function setPagination(Pagination $pagination)
    {
        $this->pagination = $pagination;
    }

    /**
     * Get Collection Pagination
     *
     * @return Pagination
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * Get rendered pagination links
     *
     * @return null|array
     */
    public function getPaginationLinks()
    {
        $pagination = array();
        foreach($this->getPagination()->getLinks() as $link)
        {
            if($link->isComplete())
            {
                $pagination[$link->getRelation()] = $link->getUrl();
            }
        }

        if(sizeof($pagination) > 0)
        {
            return $pagination;
        }

        return array();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("_links")
     */
    public function getLinkUrls()
    {
        $links = array();
        foreach($this->getLinks() as $link)
        {
            if($link->isComplete())
            {
                $links[$link->getRelation()] = $link->getUrl();
            }
        }

        return ArrayUtils::merge(
            $links,
            $this->getPaginationLinks()
        );
    }
}
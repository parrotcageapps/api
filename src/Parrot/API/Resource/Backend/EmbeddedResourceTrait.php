<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Backend;

/**
 * Class EmbeddedResourceTrait
 * @package Parrot\API\Resource\Backend
 */
trait EmbeddedResourceTrait
{
    /**
     * Get Embedded Resources
     *
     * @return mixed
     */
    abstract public function getEmbeddedResources();

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("_embedded")
     *
     * @return mixed
     */
    public function exposeEmbeddedResources()
    {
        $resources = $this->getEmbeddedResources();

        if(sizeof($resources) > 0)
        {
            return $resources;
        }
    }
} 
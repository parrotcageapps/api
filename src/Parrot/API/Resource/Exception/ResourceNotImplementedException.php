<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Exception;

/**
 * Class ResourceNotImplementedException
 * @package Parrot\API\Resource\Exception
 */
class ResourceNotImplementedException extends \Exception implements ExceptionInterface
{
    /**
     * Constructor
     *
     * @param string $message
     * @param int $code
     */
    public function __construct($message = 'Resource not implemented', $code = 501)
    {
        parent::__construct($message, $code);
    }
}
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Exception;

/**
 * Interface ExceptionInterface
 * @package Parrot\API\Resource\Exception
 */
interface ExceptionInterface {}
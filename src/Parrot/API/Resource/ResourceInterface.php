<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource;

use Parrot\API\Problem\Problem;
use Parrot\API\Problem\Response\ProblemResponse;
use Parrot\API\Resource\Backend\ResourceInterface as BackendResource;
use Parrot\API\Resource\Backend\Collection\Collection as BackendCollection;

/**
 * Interface ResourceInterface
 * @package Parrot\API\Resource
 */
interface ResourceInterface
{
    /**
     * Create a record in the Resource
     *
     * @param  array|object $data
     * @return BackendResource|Problem|ProblemResponse
     */
    public function create($data);

    /**
     * Update (replace) an existing Resource
     *
     * @param  string|int $id
     * @param  array|object $data
     * @return BackendResource|Problem|ProblemResponse
     */
    public function update($id, $data);

    /**
     * Update (replace) an existing Collection of resources
     *
     * @param  array $data
     * @return BackendCollection|Problem|ProblemResponse
     */
    public function replaceList($data);

    /**
     * Partial update of an existing Resource
     *
     * @param  string|int $id
     * @param  array|object $data
     * @return BackendResource|Problem|ProblemResponse
     */
    public function patch($id, $data);


    /**
     * Partial update of an existing collection
     *
     * @param  array|object $data
     * @return BackendCollection|Problem|ProblemResponse
     */
    public function patchList($data);

    /**
     * Delete an existing Resource
     *
     * @param  string|int $id
     * @return bool|Problem|ProblemResponse
     */
    public function delete($id);

    /**
     * Delete an existing Collection of resources
     *
     * @param  null|array $data
     * @return bool
     */
    public function deleteList($data = null);

    /**
     * Fetch an existing Resource
     *
     * @param  string|int $id
     * @return BackendResource|Problem|ProblemResponse
     */
    public function fetch($id);

    /**
     * Fetch a collection of resources
     *
     * @param $data
     * @return BackendCollection|Problem
     */
    public function fetchAll($data = array());
} 
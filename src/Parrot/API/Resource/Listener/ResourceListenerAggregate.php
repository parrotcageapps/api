<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Listener;

use Parrot\API\Resource\Backend\ResourceInterface as BackendResourceInterface;
use Parrot\API\Resource\Controller\ResourceController;
use Parrot\API\Resource\Resource;
use Parrot\API\Resource\View\Model\ResourceJsonModel;
use Zend\EventManager\Event;

/**
 * Class ResourceListenerAggregate
 *
 * @package Parrot\API\Resource\Listener
 */
class ResourceListenerAggregate extends BaseResourceListenerAggregate
{
    /**
     * Determines if we're dealing with the required Resource
     * before dispatching to actions
     *
     * @param Event $e
     * @return mixed|void
     */
    public function dispatch(Event $e)
    {
        switch($e->getName())
        {
            case 'create.post':
            case 'update.post':
            case 'patch.post':
                /**
                 * Redirect client to newly created resource
                 */
                $controller = $e->getTarget();
                if($controller instanceof ResourceController)
                {
                    $resource = $controller->getResource();
                    if($resource instanceof Resource)
                    {
                        $model = $e->getParam('resource');
                        if($model instanceof ResourceJsonModel)
                        {
                            $object = $model->getPayload();
                            if($object instanceof BackendResourceInterface)
                            {
                                $e->stopPropagation(true);
                                $response = $controller->redirect()->toRoute('api/default', array(
                                    'resource' => $resource->getIdentifier(),
                                    'id'    => $object->getId()
                                ));

                                return $response; //@TODO remove until fixed CORS redirect
                            }
                        }
                    }
                }
                break;
        }
    }
}
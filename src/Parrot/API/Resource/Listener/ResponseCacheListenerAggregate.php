<?php
 
 /**
 * CTI Digital
 *
 * @author Jason Brown <j.brown@ctidigital.com>
 */

namespace Parrot\API\Resource\Listener;

use Parrot\API\Resource\Resource;
use Parrot\API\Resource\Service\ResourceLocatorAwareInterface;
use Parrot\API\Resource\Service\ResourceLocatorAwareTrait;
use Zend\Cache\Storage\Adapter\AbstractAdapter;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Http\Header\CacheControl;
use Zend\Http\Header\Etag;
use Zend\Http\Header\Expires;
use Zend\Http\Header\IfModifiedSince;
use Zend\Http\Header\LastModified;
use Zend\Http\Header\Pragma;
use Zend\Http\Request as HttpRequest;
use Zend\Http\Response as HttpResponse;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\ArrayUtils;

/**
 * Class ResponseCacheListenerAggregate
 * @package Parrot\API\Resource\Listener
 */
class ResponseCacheListenerAggregate extends AbstractListenerAggregate implements ListenerAggregateInterface, ResourceLocatorAwareInterface
{
    use ResourceLocatorAwareTrait;

    /**
     * @var AbstractAdapter
     */
    protected $cacheAdapter;

    /**
     * @var string
     */
    private $cache_key;

    /**
     * @param AbstractAdapter $cacheAdapter
     */
    public function __construct(AbstractAdapter $cacheAdapter)
    {
        $this->cacheAdapter = $cacheAdapter;
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
       $events->attach(MvcEvent::EVENT_DISPATCH, array($this, 'onDispatch'), -1000);
       $events->attach(MvcEvent::EVENT_FINISH, array($this, 'onFinish'), -1000);
    }

    /**
     * Intercept Dispatch and determine
     * if a cached response for this route
     * is available.
     *
     * Retrieve and return response if available
     * shortcutting to Finish event
     *
     * @param MvcEvent $e
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        $request = $e->getRequest();

        if(!$request instanceof HttpRequest)
        {
            return;
        }

        if(!$request->isGet())
        {
            return;
        }

        $response = $e->getResponse();

        if($response instanceof HttpResponse && !$response->isOk())
        {
            return;
        }

        $resourceIdentifier = $e->getRouteMatch()->getParam('resource');
        $params             = ArrayUtils::merge($e->getRouteMatch()->getParams(), $request->getQuery()->toArray());
        $this->cache_key    = $this->composeKey($params);

        // Do not continue if we cannot compose a key
        if(empty($this->cache_key))
        {
            return;
        }

        if($this->cacheAdapter->hasItem($this->cache_key)){
            $cachedResponse = $this->cacheAdapter->getItem($this->cache_key);

            if($cachedResponse instanceof HttpResponse)
            {
                $resource = call_user_func($this->getResourceLocatorService(), $resourceIdentifier);

                if(!$resource instanceof Resource)
                {
                    return;
                }

                $modifiedSince = $request->getHeader('If-Modified-Since');
                if ($modifiedSince instanceof IfModifiedSince)
                {
                    $headers      = $cachedResponse->getHeaders();
                    $lastModified = $headers->get('Last-Modified');

                    if (!$lastModified instanceof LastModified) {
                        $lastModified = new LastModified();
                        $headers->addHeader($lastModified);
                    }

                    $age    = $lastModified->date()->getTimestamp() - $modifiedSince->date()->getTimestamp();
                    $maxAge = $resource->getCacheOptions()->getMaxAge();

                    if($age < $maxAge && $maxAge >= 1)
                    {
                        $lastModified->setDate($modifiedSince->date());
                        $cachedResponse->setStatusCode(HttpResponse::STATUS_CODE_304);
                        $cachedResponse->setContent(null);

                        return $cachedResponse;
                    }
                }

                //return $cachedResponse;
            }
        }
    }

    /**
     * Cache Response for future requests
     *
     * @param MvcEvent $e
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function onFinish(MvcEvent $e)
    {
        $request = $e->getRequest();

        if(!$request instanceof HttpRequest)
        {
            return;
        }

        if(!$request->isGet())
        {
            return;
        }

        $response = $e->getResponse();
        if($response instanceof HttpResponse && !$response->isOk())
        {
            return;
        }

        // Do not continue if weren't able to compose a key
        if(empty($this->cache_key))
        {
            return;
        }

        if(!$this->cacheAdapter->hasItem($this->cache_key))
        {
            $resourceIdentifier = $e->getRouteMatch()->getParam('resource');
            $resource = call_user_func($this->getResourceLocatorService(), $resourceIdentifier);

            if(!$resource instanceof Resource || !$resource->isCacheable())
            {
                return;
            }

            // Generate Response cache headers based on Resource CacheOptions
            $cacheOptions = $resource->getCacheOptions();
            $cacheControl = new CacheControl();

            $cacheControl->addDirective($cacheOptions->getAccess());
            $cacheControl->addDirective('max-age', $cacheOptions->getMaxAge());
            $cacheControl->addDirective('expires', $cacheOptions->getExpires());
            $cacheControl->addDirective('must-revalidate');

            $dateTime = new \DateTime();
            $dateTime->modify('+ '. $cacheOptions->getExpires() .'seconds');

            $expires = new Expires();
            $expires->setDate($dateTime);

            $lastModified = new LastModified();
            $lastModified->setDate(new \DateTime());

            // Add Headers to Response Header
            $response->getHeaders()->addHeader($cacheControl);
            $response->getHeaders()->addHeader($expires);
            $response->getHeaders()->addHeaderLine('Pragma: ' . $cacheOptions->getAccess());
            $response->getHeaders()->addHeader(Etag::fromString('Etag: ' . md5($response->getBody())));
            $response->getHeaders()->addHeader($lastModified);

            // Set cache adapter's TTL using Resource cache expires value
            $this->cacheAdapter->getOptions()->setTtl($cacheOptions->getExpires());
            $this->cacheAdapter->setItem($this->cache_key, $response);

            //return $response;
        }
    }

    /**
     * Compose Cache Key
     *
     * @param $params
     * @return string
     */
    protected function composeKey($params)
    {
        unset($params['controller']);
        unset($params['action']);
        unset($params['access_token']);

        $key = "response";
        foreach($params as $index => $value)
        {
            $key .= "_" . $index . "_" . $value;
        }

        return $key;
    }
}
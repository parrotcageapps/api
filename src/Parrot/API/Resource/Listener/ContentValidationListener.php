<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Listener;

use Parrot\API\Problem\Problem;
use Parrot\API\Problem\Response\ProblemResponse;
use Parrot\API\Resource\Controller\ResourceController;
use Parrot\API\Resource\Resource;
use Parrot\API\Resource\Service\ResourceLocatorAwareTrait;
use Zend\EventManager\Event;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\Http\Request as HttpRequest;
use Zend\InputFilter\InputFilter;
use Zend\Mvc\Router\Http\RouteMatch;

/**
 * Class ContentValidationListener
 * @package Parrot\API\Resource\Listener
 */
class ContentValidationListener implements SharedListenerAggregateInterface
{
    /**
     * @var array
     */
    protected $methodsWithoutBodies = array(
        'GET',
        'HEAD',
        'OPTIONS',
        'DELETE',
    );

    /**
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $sharedListeners = array();

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the SharedEventManager
     * implementation will pass this to the aggregate.
     *
     * @param SharedEventManagerInterface $events
     */
    public function attachShared(SharedEventManagerInterface $events)
    {
        $events->attach('*', array(
            'create.pre',
            'update.pre',
            'replaceList.pre',
            'patch.pre',
            'patchList.pre'
            ),
            array($this, 'validateContent'),
            -500 // Should be the last Event to be fired before data is passed to Backend
        );
    }

    /**
     * Detach all previously attached listeners
     *
     * @param SharedEventManagerInterface $events
     */
    public function detachShared(SharedEventManagerInterface $events)
    {
        foreach ($this->sharedListeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->sharedListeners[$index]);
            }
        }
    }

    /**
     * Validate and Filter content passed to the Resource
     *
     * By default this will prevent XSS and malicious
     * code being passed to the backend.
     *
     * Further event propagation is stopped to prevent
     * additional manipulation to reduce further risk.
     *
     * @param Event $event
     * @return array|null
     */
    public function validateContent(Event $event)
    {
        $controller = $event->getTarget();
        if($controller instanceof ResourceController)
        {
            $request = $event->getTarget()->getRequest();
            if (!$request instanceof HttpRequest) {
                return;
            }

            if (in_array($request->getMethod(), $this->methodsWithoutBodies)) {
                return;
            }

            $resource = $controller->getResource();
            if($resource instanceof Resource)
            {
                /**
                 * Check Request method is allowed by Resource
                 * Usually this would be picked up by Access Control,
                 * however we should always check
                 */
                if(!in_array($request->getMethod() ,$resource->getResourceHttpMethods()))
                {
                    return;
                }

                $inputFilter  = $resource->getInputFilter();
                if(!$inputFilter instanceof InputFilter)
                {
                    return;
                }

                try {
                    $data = $event->getParam('data', array());

                    if($request->isPatch())
                    {
                        // Only filter values that have been provided
                        $inputFilter->setValidationGroup(array_keys($data));
                    }

                    $inputFilter->setData($data);
                    if($inputFilter->isValid())
                    {
                        $filteredData = $inputFilter->getValues();
                        $event->stopPropagation(true);

                        return array_merge($data, $filteredData);
                    }

                    return new ProblemResponse(
                        new Problem(422, 'Failed Validation', null, null, [
                            'validation_messages' => $inputFilter->getMessages(),
                        ])
                    );
                } catch (\Exception $ex) {
                    return new ProblemResponse(
                        new Problem(400, 'Invalid data specified in request')
                    );
                }
            }
        }
    }
}
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Listener;

use Parrot\API\Resource\Controller\ResourceController;
use Parrot\API\Resource\Resource;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\Mvc\MvcEvent;

/**
 * Class RestParameterListener
 * @package Parrot\API\Resource\Listener
 */
class RestParameterListener extends AbstractListenerAggregate implements SharedListenerAggregateInterface
{
    /**
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $sharedListeners = array();

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('dispatch', array($this, 'onDispatch'), 100);
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the SharedEventManager
     * implementation will pass this to the aggregate.
     *
     * @param SharedEventManagerInterface $events
     */
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->sharedListeners[] = $events->attach(
                                                    '*',
                                                    'dispatch',
                                                    array($this, 'onDispatch'),
                                                    100);
    }

    /**
     * Detach all previously attached listeners
     *
     * @param SharedEventManagerInterface $events
     */
    public function detachShared(SharedEventManagerInterface $events)
    {
        foreach ($this->sharedListeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->sharedListeners[$index]);
            }
        }
    }

    /**
     * Listen to the dispatch event
     *
     * @param MvcEvent $e
     */
    public function onDispatch(MvcEvent $e)
    {
        $controller = $e->getTarget();
        if (!$controller instanceof ResourceController) {
            return;
        }

        $request  = $e->getRequest();
        $query    = $request->getQuery();
        $resource = $controller->getResource();

        if(!$resource instanceof Resource)
        {
            $resource = $controller->locateResource($e->getRouteMatch()->getParam('resource'));
            if($resource instanceof Resource)
            {
                $resource->setQueryParams($query);
                $resource->setRouteMatch($e->getRouteMatch());
            }
        }else{
            $resource->setQueryParams($query);
            $resource->setRouteMatch($e->getRouteMatch());
        }
    }
}
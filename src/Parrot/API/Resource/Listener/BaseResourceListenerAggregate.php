<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Listener;

use Parrot\API\Resource\Controller\ResourceController;
use Zend\EventManager\Event;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\SharedListenerAggregateInterface;

/**
 * Class BaseResourceListenerAggregate
 * @package Parrot\API\Resource\Listener
 */
class BaseResourceListenerAggregate implements SharedListenerAggregateInterface
{
    /**
     * @var string
     */
    protected $resourceIdentifier = 'default';

    /**
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $sharedListeners = array();

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the SharedEventManager
     * implementation will pass this to the aggregate.
     *
     * @param SharedEventManagerInterface $events
     */
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->sharedListeners[] = $events->attach('*',
            array(
                'get.pre',
                'get.post',
                'getList.pre',
                'getList.post',
                'create.pre',
                'create.post',
                'update.pre',
                'update.post',
                'patch.pre',
                'patch.post',
                'delete.pre',
                'delete.post',
                'update.pre',
                'update.post',
            ),
            array($this, 'dispatch')
        );
    }

    /**
     * Detach all previously attached listeners
     *
     * @param SharedEventManagerInterface $events
     */
    public function detachShared(SharedEventManagerInterface $events)
    {
        foreach ($this->sharedListeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->sharedListeners[$index]);
            }
        }
    }

    /**
     * Determines if we're dealing with the required Resource
     * before dispatching to actions
     *
     * @param Event $e
     * @return boolean
     */
    public function dispatch(Event $e)
    {
        $controller = $e->getTarget();
        if(!$controller instanceof ResourceController)
        {
            return false;
        }

        $resource           = $controller->getResource();
        $resourceIdentifier = $resource->getIdentifier();
        if($resourceIdentifier !== $this->resourceIdentifier)
        {
            return false;
        }

        return true;
    }
}
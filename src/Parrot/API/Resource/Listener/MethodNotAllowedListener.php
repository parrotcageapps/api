<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Listener;

use Parrot\API\Problem\Problem;
use Parrot\API\Problem\Response\ProblemResponse;
use Parrot\API\Resource\Controller\ResourceController;
use Parrot\API\Resource\Resource;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\ArrayUtils;

/**
 * Class MethodNotAllowedListener
 * @package Parrot\API\Resource\Listener
 *
 * Determines if a Method is Allowed by the Resource/Collection
 */
class MethodNotAllowedListener extends AbstractListenerAggregate implements SharedListenerAggregateInterface
{
    /**
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $sharedListeners = array();

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, array($this, 'onDispatch'), 200);
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the SharedEventManager
     * implementation will pass this to the aggregate.
     *
     * @param SharedEventManagerInterface $events
     */
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->sharedListeners[] = $events->attach(
            '*',
            MvcEvent::EVENT_DISPATCH,
            array($this, 'onDispatch'),
            200);
    }

    /**
     * Detach all previously attached listeners
     *
     * @param SharedEventManagerInterface $events
     */
    public function detachShared(SharedEventManagerInterface $events)
    {
        foreach ($this->sharedListeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->sharedListeners[$index]);
            }
        }
    }

    public function onDispatch(MvcEvent $e)
    {
        $controller = $e->getTarget();
        if (!$controller instanceof ResourceController) {
            return;
        }

        $resource = ($controller->getResource()instanceof Resource) ? $controller->getResource() : $controller->locateResource($e->getRouteMatch()->getParam('resource'));

        if($resource instanceof Resource)
        {
            if(!in_array($e->getRequest()->getMethod(), $resource->getResourceHttpMethods()))
            {
                return new ProblemResponse(new Problem(405, 'Method not allowed'));
            }
        }
    }
}
<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource;

use ArrayObject;
use Parrot\API\Problem\Exception\DomainException;
use Parrot\API\Problem\Problem;
use Parrot\API\Problem\Response\ProblemResponse;
use Parrot\API\Resource\Backend\BaseBackend;
use Parrot\API\Resource\Backend\EmbeddedResourcesInterface;
use Parrot\API\Resource\Backend\ResourceInterface as BackendResource;
use Parrot\API\Resource\Backend\Collection\Collection as BackendCollection;
use Parrot\API\Resource\Cache\CacheOptions;
use Parrot\API\Resource\Link\Link;
use Parrot\API\Resource\Link\LinkCollectionAwareInterface;
use Zend\Cache\Storage\Adapter\AbstractAdapter;
use Zend\Cache\Storage\Adapter\Memory;
use Zend\Cache\Storage\Adapter\Session;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\EventManager\EventManagerInterface;
use Zend\InputFilter\InputFilter;
use Zend\Mvc\Router\RouteMatch;
use Zend\Session\Container;
use Zend\Stdlib\ArrayUtils;
use Zend\Stdlib\Parameters;
use Zend\View\Helper\ServerUrl;
use Zend\View\Helper\Url;

/**
 * Class Resource
 * @package Parrot\API\Resource
 */
class Resource implements ResourceInterface, EventManagerAwareInterface
{
    /**
     * HTTP methods we allow for individual resources; used by options()
     *
     * HEAD and OPTIONS are always available.
     *
     * @var array
     */
    protected $resourceHttpMethods = array(
        'DELETE',
        'GET',
        'PATCH',
        'PUT',
        'POST',
        'OPTIONS',
        'HEAD'
    );

    /**
     * @TODO use a required scope array to identify what is required to perform requests on this resource
     * default scope is resource identifier, action to update could be edit_article = PATCH
     */

    /**
     * Identifies the Resource
     *
     * @var string
     */
    protected $identifier;

    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var array
     */
    protected $params = array();

    /**
     * @var null|Parameters
     */
    protected $queryParams;

    /**
     * @var null|RouteMatch
     */
    protected $routeMatch;

    /**
     * Array of Resource Backends to provide resources
     *
     * @var array
     */
    protected $resourceBackends = array();

    /**
     * Input Filter used to validate Resource
     *
     * @var InputFilter
     */
    protected $inputFilter;

    /**
     * @var Url
     */
    protected $urlHelper;

    /**
     * @var ServerUrl
     */
    protected $serverUrlHelper;

    /**
     * Whether Resource/Collection links should be rendered
     *
     * @var bool
     */
    protected $renderLinks = true;

    /**
     * Caching Options for this Resource
     *
     * @var Cache\CacheOptions
     */
    protected $cacheOptions;

    /**
     * Cache Adapter
     *
     * @var AbstractAdapter
     */
    protected $cacheAdapter;

    /**
     * @param null $identifier
     * @throws \Parrot\API\Problem\Exception\DomainException
     */
    public function __construct($identifier = null)
    {
        $this->identifier = $identifier;
    }

    /**
     * Create a record in the resource
     *
     * @param  array $data
     * @return BackendResource|Problem|ProblemResponse
     */
    public function create($data)
    {
        $events  = $this->getEventManager();
        $event   = $this->prepareEvent(__FUNCTION__, array('data' => $data));
        $results = $events->triggerUntil($event, function($result) {
            return ($result instanceof Problem
                || $result instanceof ProblemResponse
            );
        });
        $last = $results->last();
        if (!is_array($last) && !is_object($last)) {
            return $data;
        }

        return $last;
    }

    /**
     * Update (replace) an existing resource
     *
     * @param  string|int $id
     * @param  array|object $data
     * @return BackendResource|Problem|ProblemResponse
     */
    public function update($id, $data)
    {
        $events  = $this->getEventManager();
        $event   = $this->prepareEvent(__FUNCTION__, array('id' => $id, 'data' => $data));
        $results = $events->triggerUntil($event, function($result) {
            return ($result instanceof Problem
                || $result instanceof ProblemResponse
            );
        });
        $last = $results->last();
        if (!is_array($last) && !is_object($last)) {
            return $data;
        }

        // Check cache for Resource
        if($this->getCacheAdapter()->hasItem($this->getIdentifier() . '_' . $id))
        {
            $this->getCacheAdapter()->setItem($this->getIdentifier() . '_' . $id, $last);
        }

        return $last;
    }

    /**
     * Update (replace) an existing collection of resources
     *
     * @param  array $data
     * @return BackendCollection|Problem|ProblemResponse
     */
    public function replaceList($data)
    {
        $events   = $this->getEventManager();
        $event    = $this->prepareEvent(__FUNCTION__, array('data' => $data));
        $results  = $events->triggerUntil($event, function($result) {
            return ($result instanceof Problem
                || $result instanceof ProblemResponse
            );
        });
        $last = $results->last();
        if (!is_array($last) && !is_object($last)) {
            return $data;
        }
        return $last;
    }

    /**
     * Partial update of an existing resource
     *
     * @param  string|int $id
     * @param  array|object $data
     * @return BackendResource|Problem|ProblemResponse
     */
    public function patch($id, $data)
    {
        $events   = $this->getEventManager();
        $event    = $this->prepareEvent(__FUNCTION__, array('id' => $id, 'data' => $data));
        $results  = $events->triggerUntil($event, function($result) {
            return ($result instanceof Problem
                || $result instanceof ProblemResponse
            );
        });
        $last = $results->last();
        if (!is_array($last) && !is_object($last)) {
            return $data;
        }

        // Check cache for Resource
        if($this->getCacheAdapter()->hasItem($this->getIdentifier() . '_' . $id))
        {
            $this->getCacheAdapter()->setItem($this->getIdentifier() . '_' . $id, $last);
        }

        return $last;
    }

    /**
     * Partial update of an existing collection
     *
     * @param  array|object $data
     * @return BackendCollection|Problem|ProblemResponse
     */
    public function patchList($data)
    {
        $events   = $this->getEventManager();
        $event    = $this->prepareEvent(__FUNCTION__, array('data' => $data));
        $results  = $events->triggerUntil($event, function($result) {
            return ($result instanceof Problem
                || $result instanceof ProblemResponse
            );
        });
        $last = $results->last();
        if (!is_array($last) && !is_object($last)) {
            return $data;
        }
        return $last;
    }

    /**
     * Delete an existing resource
     *
     * @param  string|int $id
     * @return bool|Problem|ProblemResponse
     */
    public function delete($id)
    {
        $events  = $this->getEventManager();
        $event   = $this->prepareEvent(__FUNCTION__, array('id' => $id));
        $results = $events->triggerUntil($event, function($result) {
            return ($result instanceof Problem
                || $result instanceof ProblemResponse
            );
        });
        $last = $results->last();
        if (!is_bool($last) && (!$last instanceof Problem) && (!$last instanceof ProblemResponse)) {
            return false;
        }

        // Check cache for Resource
        if($this->getCacheAdapter()->hasItem($this->getIdentifier() . '_' . $id))
        {
            $this->getCacheAdapter()->removeItem($this->getIdentifier() . '_' . $id);
        }

        return $last;
    }

    /**
     * Delete an existing collection of resources
     *
     * @param  null|array $data
     * @return bool
     */
    public function deleteList($data = null)
    {
        $events  = $this->getEventManager();
        $event   = $this->prepareEvent(__FUNCTION__, array('data' => $data));
        $results = $events->triggerUntil($event, function($result) {
            return ($result instanceof Problem
                || $result instanceof ProblemResponse
            );
        });
        $last = $results->last();
        if (!is_bool($last) && (!$last instanceof Problem) && (!$last instanceof ProblemResponse)) {
            return false;
        }
        return $last;
    }

    /**
     * Fetch an existing resource
     *
     * @param  string|int $id
     * @return BackendResource|Problem|ProblemResponse
     */
    public function fetch($id)
    {
        // Check cache for Resource
        if($this->getCacheAdapter()->hasItem($id))
        {
            return $this->getCacheAdapter()->getItem($id);
        }

        $events  = $this->getEventManager();
        $event   = $this->prepareEvent(__FUNCTION__, array('id' => $id));
        $results = $events->triggerUntil($event, function($result) {
            return ($result instanceof Problem
                || $result instanceof ProblemResponse || $result instanceof BackendResource
            );
        });

        $last = $results->last();

        if (!$last instanceof BackendResource) {
            // Resource Backend hasn't returned a Resource
            return new Problem(404, 'Resource not found');
        }

        // Render Resource
        $this->renderResource($last);

        // Store Resource in cache
        $this->getCacheAdapter()->setItem($this->getIdentifier() . '_' . $id, $last);

        return $last;
    }

    /**
     * Fetch a Collection of resources
     *
     * @param array $data
     * @return BackendCollection|Problem
     */
    public function fetchAll($data = array())
    {
        $events  = $this->getEventManager();
        $event   = $this->prepareEvent(__FUNCTION__, array('data' => $data));
        $results = $events->triggerUntil($event, function($result) {
            return ($result instanceof Problem
                || $result instanceof ProblemResponse || $result instanceof BackendResource
            );
        });

        $last = $results->last();

        if($last instanceof Problem || $last instanceof ProblemResponse)
        {
            return $last;
        }

        if (!$last instanceof BackendCollection) {
            // Resource Backend hasn't returned a Collection
            return new Problem(404, 'Collection not found');
        }

        // Render Collection
        $this->renderCollection($last);

        // Prepare pagination links
        $last->getPagination()->composeLinksFromResourceEvent($event);
        foreach($last->getPagination()->getLinks() as $link)
        {
            $this->renderUrlForLink($link);
        }

        return $last;
    }

    /**
     * Render a Collection
     *
     * @param BackendCollection $collection
     */
    public function renderCollection(BackendCollection $collection)
    {
        if($collection instanceof LinkCollectionAwareInterface)
        {
            $this->injectSelfLink($collection);

            $resources = $collection->getResources();
            array_walk_recursive($resources, array($this, 'renderResource'));
        }
    }

    /**
     * Render a Resource
     *
     * @param BackendResource|array $resource
     */
    public function renderResource($resource)
    {
        if($resource instanceof LinkCollectionAwareInterface)
        {
            $this->injectSelfLink($resource);

            // Render additional Resource Links
            foreach($resource->getlinks() as $link)
            {
                $this->renderUrlForLink($link);
            }

            if($resource instanceof EmbeddedResourcesInterface)
            {
                // Render Embedded Resources
                $resources = $resource->getEmbeddedResources();
                //@TODO fix recursive resource links
                array_walk_recursive($resources, array($this, 'renderResource'));
            }
        }
    }

    /**
     * Provides an array of allowed methods for a Resource
     *
     * @return array
     */
    public function options()
    {
        return $this->getResourceHttpMethods();
    }

    /**
     * Set the Resource identifier
     *
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * Get the Resource identifier
     *
     * @return null|string
     * @throws \Parrot\API\Problem\Exception\DomainException
     */
    public function getIdentifier()
    {
        if($this->identifier === null)
        {
            throw new DomainException(sprintf(
                '%s requires an identifier',
                __CLASS__
            ));
        }

        return $this->identifier;
    }

    /**
     * Set Allowed Http Methods for this Resource
     *
     * @param array $resourceHttpMethods
     */
    public function setResourceHttpMethods(array $resourceHttpMethods)
    {
        // Ensure we always allow OPTIONS method
        if(!in_array('OPTIONS', $resourceHttpMethods))
        {
            $resourceHttpMethods[] = 'OPTIONS';
        }

        // Ensure we always allow HEAD method
        if(!in_array('HEAD', $resourceHttpMethods))
        {
            $resourceHttpMethods[] = 'HEAD';
        }

        $this->resourceHttpMethods = $resourceHttpMethods;
    }

    /**
     * Get Allowed Http Methods for this Resource
     *
     * @return array
     */
    public function getResourceHttpMethods()
    {
        return $this->resourceHttpMethods;
    }

    /**
     * Set Event Params
     *
     * @param array $params
     * @return self
     */
    public function setEventParams(array $params)
    {
        $this->params = $params;
        return $this;
    }

    /**
     * Get Event Params
     *
     * @return array
     */
    public function getEventParams()
    {
        return $this->params;
    }

    /**
     * Set Query Params
     *
     * @param Parameters $params
     * @return self
     */
    public function setQueryParams(Parameters $params)
    {
        $this->queryParams = $params;
        return $this;
    }

    /**
     * Get Query Params
     *
     * @return null|Parameters
     */
    public function getQueryParams()
    {
        return $this->queryParams;
    }

    /**
     * Set Route Match
     *
     * @param RouteMatch $matches
     * @return self
     */
    public function setRouteMatch(RouteMatch $matches)
    {
        $this->routeMatch = $matches;
        return $this;
    }

    /**
     * Get Route Match
     *
     * @return null|RouteMatch
     */
    public function getRouteMatch()
    {
        return !is_null($this->routeMatch) ? $this->routeMatch : new RouteMatch($this->getEventParams());
    }

    /**
     * Set Event Param
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return mixed
     */
    public function setEventParam($name, $value)
    {
        $this->params[$name] = $value;
        return $this;
    }

    /**
     * Get Event Param
     *
     * @param mixed $name
     * @param mixed $default
     *
     * @return mixed
     */
    public function getEventParam($name, $default = null)
    {
        if (isSet($this->params[$name])) {

            return $this->params[$name];
        }

        return $default;
    }

    /**
     * Register a Resource Backend with this Resource
     *
     * @param BaseBackend $resourceBackend
     * @param $identifier - uniquely identifies a Resource Backend;
     *                      useful when registering another instance of the same Resource Backend.
     */
    public function registerResourceBackend($resourceBackend, $identifier = null)
    {
        $identifier = $identifier ?: get_class($resourceBackend);

        $this->resourceBackends[$identifier] = $resourceBackend;
    }

    /**
     * Get a Resource Backend
     *
     * @param $identifier
     * @return BaseBackend
     */
    public function getResourceBackend($identifier)
    {
        return $this->resourceBackends[$identifier];
    }

    /**
     * Get all Resource Backends registered with Resource
     *
     * @return array
     */
    public function getResourceBackends()
    {
        return $this->resourceBackends;
    }

    /**
     * Set event manager instance
     *
     * Sets the event manager identifiers to the current class, this class, and
     * the resource interface.
     *
     * @param  EventManagerInterface $events
     * @return Resource
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            get_class($this),
            __CLASS__,
            'Parrot\API\Resource\ResourceInterface',
        ));
        $this->events = $events;

        foreach($this->getResourceBackends() as $backend)
        {
            $this->getEventManager()->attachAggregate(
                $backend,
                $backend->getPriority()
            );
        }

        return $this;
    }

    /**
     * Retrieve event manager
     *
     * Lazy-instantiates an EM instance if none provided.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }

    /**
     * Set Input Filter
     *
     * @param \Zend\InputFilter\InputFilter $resourceInputFilter
     */
    public function setInputFilter($resourceInputFilter)
    {
        $this->inputFilter = $resourceInputFilter;
    }

    /**
     * Get Input Filter
     *
     * @return \Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        return $this->inputFilter ?: new InputFilter();
    }

    /**
     * Set Url Helper
     *
     * @param \Zend\View\Helper\ServerUrl $serverUrlHelper
     */
    public function setServerUrlHelper($serverUrlHelper)
    {
        $this->serverUrlHelper = $serverUrlHelper;
    }

    /**
     * Get Server Url Helper
     *
     * @return \Zend\View\Helper\ServerUrl
     */
    public function getServerUrlHelper()
    {
        return $this->serverUrlHelper;
    }

    /**
     * Set Url Helper
     *
     * @param \Zend\View\Helper\Url $urlHelper
     */
    public function setUrlHelper($urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

    /**
     * Get Url Helper
     *
     * @return \Zend\View\Helper\Url
     */
    public function getUrlHelper()
    {
        return $this->urlHelper;
    }

    /**
     * @param boolean $renderLinks
     */
    public function setRenderLinks($renderLinks)
    {
        $this->renderLinks = $renderLinks;
    }

    /**
     * @return boolean
     */
    public function getRenderLinks()
    {
        return $this->renderLinks;
    }

    /**
     * Set Resource Cache Options
     *
     * @param \Parrot\API\Resource\Cache\CacheOptions $cacheOptions
     */
    public function setCacheOptions($cacheOptions)
    {
        $this->cacheOptions = $cacheOptions;
    }

    /**
     * Get Resource Cache Options
     *
     * @return \Parrot\API\Resource\Cache\CacheOptions
     */
    public function getCacheOptions()
    {
        if(is_null($this->cacheOptions))
        {
            $this->cacheOptions = new CacheOptions();
        }

        return $this->cacheOptions;
    }

    /**
     * Check if a Resource should be cached
     *
     * @return bool
     */
    public function isCacheable()
    {
        // If No-Cache true Resource cannot be cached
        if($this->getCacheOptions()->getNoCache())
        {
            return false;
        }

        return true;
    }

    /**
     * Set Cache Adapter
     *
     * @param \Zend\Cache\Storage\Adapter\AbstractAdapter $cacheAdapter
     */
    public function setCacheAdapter($cacheAdapter)
    {
        $this->cacheAdapter = $cacheAdapter;
        $this->cacheAdapter->getOptions()->setTtl($this->getCacheOptions()->getExpires());
    }

    /**
     * Get Cache Adapter
     *
     * @return \Zend\Cache\Storage\Adapter\AbstractAdapter
     */
    public function getCacheAdapter()
    {
        if(is_null($this->cacheAdapter))
        {
            $session   = new Session();
            $container = new Container();

            $session->getOptions()->setSessionContainer($container);
            $this->setCacheAdapter($session);
        }

        return $this->cacheAdapter;
    }

    /**
     * Prepare event parameters
     *
     * Merges any event parameters set in the resources with arguments passed
     * to a resource method, and passes them to the `prepareArgs` method of the
     * event manager.
     *
     * @param $name
     * @param array $args
     * @return ResourceEvent
     */
    protected function prepareEvent($name, array $args)
    {
        $event = new ResourceEvent($name, $this, $this->prepareEventParams($args));
        $event->setQueryParams($this->getQueryParams());
        $event->setRouteMatch($this->getRouteMatch());
        $event->setResourceIdentifier($this->getIdentifier());

        return $event;
    }

    /**
     * Prepare event parameters
     *
     * Ensures event parameters are created as an array object, allowing them to be modified
     * by listeners and retrieved.
     *
     * @param  array $args
     * @return ArrayObject
     */
    protected function prepareEventParams(array $args)
    {
        $defaultParams = $this->getEventParams();
        $params        = array_merge($defaultParams, $args);
        if (empty($params)) {
            return $params;
        }

        return $this->getEventManager()->prepareArgs($params);
    }

    /**
     * Inject a "self" link
     *
     * @param LinkCollectionAwareInterface $resource
     */
    protected function injectSelfLink(LinkCollectionAwareInterface $resource)
    {
        if($this->renderLinks)
        {
            if(!$resource->getLinks()->has('self'))
            {
                $self               = new Link('self');
                $resourceIdentifier = ($resource instanceof BackendResource)? $resource->getIdentifier() : $this->getIdentifier();
                $params             = $this->getRouteMatch()->getParams();
                $params['resource'] = $resourceIdentifier;

                if($resource instanceof BackendResource)
                {
                    $params['id'] = $resource->getId();
                }

                $self->setRoute($this->getRouteMatch()->getMatchedRouteName());
                $self->setRouteParams($params);
                $this->renderUrlForLink($self);
                $resource->getLinks()->add($self);
            }
        }
    }

    /**
     * Render Link Url within a Link Collection
     *
     * @param Link $link
     */
    protected function renderUrlForLink(Link $link)
    {
        if(!$link->isComplete() && $this->renderLinks)
        {
            $linkUrl = call_user_func(
                $this->getUrlHelper(),
                $link->getRoute(),
                $link->getRouteParams(),
                $link->getRouteOptions()
            );

            $url = call_user_func($this->getServerUrlHelper(), $linkUrl);
            $link->setUrl($url);
        }
    }
}
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Link;

/**
 * Interface LinkCollectionAwareInterface
 * @package Parrot\API\Resource\Backend\Collection
 */
interface LinkCollectionAwareInterface
{
    /**
     * Set Link Collection
     *
     * @param LinkCollection $links
     */
    public function setLinks(LinkCollection $links);

    /**
     * Get Link Collection
     *
     * @return LinkCollection
     */
    public function getLinks();
} 
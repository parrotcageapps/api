<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Link;

use JMS\Serializer\Annotation as JMS;

/**
 * Class LinkCollectionAwareTrait
 * @package Parrot\API\Resource\Link
 */
trait LinkCollectionAwareTrait
{
    /**
     * @var LinkCollection
     */
    protected $links;

    /**
     * Set Link Collection
     *
     * @param LinkCollection $links
     */
    public function setlinks(LinkCollection $links)
    {
        $this->links = $links;
    }

    /**
     * Get Link Collection
     *
     * @return LinkCollection
     */
    public function getLinks()
    {
        if(is_null($this->links))
        {
            $this->links = new LinkCollection();
        }

        return $this->links;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("_links")
     */
    public function getLinkUrls()
    {
        $links = array();
        foreach($this->getLinks() as $link)
        {
            if($link->isComplete())
            {
                $links[$link->getRelation()] = $link->getUrl();
            }
        }

        return (sizeof($links) > 0) ? $links: null;
    }
} 
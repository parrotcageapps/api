<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Link;

use Parrot\API\Problem\Exception\InvalidArgumentException;
use Zend\Uri\Uri;


/**
 * Class Link
 * @package Parrot\API\Resource\Link
 */
class Link
{
    /**
     * @var string
     */
    protected $relation;

    /**
     * @var Uri
     */
    protected $uri;

    /**
     * @var string
     */
    protected $route;

    /**
     * @var array
     */
    protected $routeParams = array();

    /**
     * @var array
     */
    protected $routeOptions = array();

    /**
     * Flag used to determine if Uri has been completed
     *
     * @var bool
     */
    protected $complete = false;

    /**
     * Constructs a Link
     *
     * @param $relation
     * @param $url
     * @throws InvalidArgumentException
     */
    public function __construct($relation, $url = null)
    {
        $this->relation = $relation;

        if(!is_null($url))
        {
            try{
                $this->uri = new Uri($url);
            }catch (\Zend\Uri\Exception\InvalidArgumentException $e){
                throw new InvalidArgumentException();
            }
        }
    }

    /**
     * @param string $relation
     */
    public function setRelation($relation)
    {
        $this->relation = $relation;
    }

    /**
     * @return string
     */
    public function getRelation()
    {
        return $this->relation;
    }

    /**
     * @param string $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function setRouteParams(array $params)
    {
        $this->routeParams = $params;
        return $this;
    }

    /**
     * @return array
     */
    public function getRouteParams()
    {
        return $this->routeParams;
    }

    /**
     * @param array $routeOptions
     * @return $this
     */
    public function setRouteOptions(array $routeOptions)
    {
        $this->routeOptions = $routeOptions;
        return $this;
    }

    /**
     * @return array
     */
    public function getRouteOptions()
    {
        return $this->routeOptions;
    }

    /**
     * Set Url
     *
     * @param $url
     * @throws \Parrot\API\Problem\Exception\InvalidArgumentException
     */
    public function setUrl($url)
    {
        try{
            $this->uri = new Uri($url);
            $this->complete = true;
        }catch (\Zend\Uri\Exception\InvalidArgumentException $e){
            throw new InvalidArgumentException();
        }
    }

    /**
     * Has the Link Uri been completed?
     *
     * @return bool
     */
    public function isComplete()
    {
        return $this->complete;
    }

    /**
     * Get Url
     *
     * @return string
     */
    public function getUrl()
    {
        if($this->uri instanceof Uri)
        {
            $url = array(
                'href' => $this->uri->toString()
            );

            $query = $this->uri->getQueryAsArray();
            if(sizeof($query) > 0)
                $url['query'] = $query;

            return $url;
        }

        return;
    }
} 
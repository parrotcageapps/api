<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Link;

use ArrayIterator;

/**
 * Class LinkCollection
 * @package Parrot\API\Resource\Link
 */
class LinkCollection implements \Countable, \IteratorAggregate
{
    /**
     * @var array
     */
    protected $links = array();

    /**
     * Retrieve an external iterator
     *
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->links);
    }

    /**
     * Count elements of an object
     *
     * @return int
     */
    public function count()
    {
        return count($this->links);
    }

    /**
     * Add Link
     *
     * @param Link $link
     * @return $this
     */
    public function add(Link $link)
    {
        $this->links[$link->getRelation()] = $link;
        return $this;
    }

    /**
     * Retrieve a link relation
     *
     * @param  string $relation
     * @return Link
     */
    public function get($relation)
    {
        if (!$this->has($relation)) {
            return null;
        }
        return $this->links[$relation];
    }

    /**
     * Does a given link relation exist?
     *
     * @param  string $relation
     * @return bool
     */
    public function has($relation)
    {
        return array_key_exists($relation, $this->links);
    }

    /**
     * Remove a given link relation
     *
     * @param  string $relation
     * @return bool
     */
    public function remove($relation)
    {
        if (!$this->has($relation)) {
            return false;
        }
        unset($this->links[$relation]);
        return true;
    }
}
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\View\Model;

use Parrot\API\Resource\Backend\Collection\Collection;
use Parrot\API\Resource\Backend\ResourceInterface;
use Zend\View\Model\JsonModel;

/**
 * Class ResourceJsonModel
 * @package Parrot\API\Resource\View\Model
 */
class ResourceJsonModel extends JsonModel
{
    /**
     * Does the payload represent a Backend Resource collection?
     *
     * @return bool
     */
    public function isCollection()
    {
        $payload = $this->getPayload();
        return ($payload instanceof Collection);
    }

    /**
     * Does the payload represent a Backend Resource item?
     *
     * @return bool
     */
    public function isResource()
    {
        $payload = $this->getPayload();
        return ($payload instanceof ResourceInterface);
    }

    /**
     * Set the payload for the response
     *
     * This is the value to represent in the response.
     *
     * @param  mixed $payload
     * @return self
     */
    public function setPayload($payload)
    {
        $this->setVariable('payload', $payload);
        return $this;
    }

    /**
     * Retrieve the payload for the response
     *
     * @return mixed
     */
    public function getPayload()
    {
        return $this->getVariable('payload');
    }
}
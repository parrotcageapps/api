<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */
 
 /**
 * CTI Digital
 *
 * @author Jason Brown <j.brown@ctidigital.com>
 */

namespace Parrot\API\Resource\View\Strategy;

use Parrot\API\Resource\View\Model\ResourceJsonModel;
use Parrot\API\Resource\View\Renderer\ResourceJsonRenderer;
use Zend\View\ViewEvent;

class ResourceJsonHalStrategy extends ResourceJsonStrategy
{
    protected $contentType = 'application/json';

    /**
     * Inject the response
     *
     * Injects the response with the rendered content, and sets the content
     * type based on the detection that occurred during renderer selection.
     *
     * @param  ViewEvent $e
     */
    public function injectResponse(ViewEvent $e)
    {
        $renderer = $e->getRenderer();
        if (!$renderer instanceof ResourceJsonRenderer) {
            return;
        }

        $result   = $e->getResult();
        if (!is_string($result)) {
            return;
        }

        $model       = $e->getModel();
        $contentType = $this->contentType;
        $response    = $e->getResponse();

        if ($model instanceof ResourceJsonModel
            && ($model->isCollection() || $model->isResource())
        ) {
            $contentType = 'application/hal+json';
        }

        // Populate response
        $response->setContent($result);
        $headers = $response->getHeaders();
        $headers->addHeaderLine('content-type', $contentType);
    }
} 
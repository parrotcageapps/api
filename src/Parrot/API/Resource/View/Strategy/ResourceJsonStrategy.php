<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\View\Strategy;

use Parrot\API\Resource\View\Model\ResourceJsonModel;
use Parrot\API\Resource\View\Renderer\ResourceJsonRenderer;
use Zend\View\Strategy\JsonStrategy;
use Zend\View\ViewEvent;

/**
 * Class ResourceJsonStrategy
 * @package Parrot\API\Resource\View\Strategy
 */
class ResourceJsonStrategy extends JsonStrategy
{
    public function __construct(ResourceJsonRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * Detect if we should use the ResourceJsonRenderer based on model type
     *
     * @param ViewEvent $e
     * @return null|ResourceJsonRenderer|\Zend\View\Renderer\JsonRenderer
     */
    public function selectRenderer(ViewEvent $e)
    {
        $model = $e->getModel();

        if(!$model instanceof ResourceJsonModel)
        {
            // Not a Resource Json Model
            return;
        }

        return $this->renderer;
    }
} 
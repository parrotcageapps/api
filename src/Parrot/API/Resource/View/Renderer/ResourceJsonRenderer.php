<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\View\Renderer;

use Parrot\API\Resource\Renderer\Plugin\PluginAwareInterface;
use Parrot\API\Resource\Renderer\Plugin\PluginAwareTrait;
use Parrot\API\Resource\View\Model\ResourceJsonModel;
use Zend\View\Renderer\JsonRenderer;

/**
 * Class ResourceJsonRenderer
 * @package Parrot\API\Resource\View\Renderer
 */
class ResourceJsonRenderer extends JsonRenderer implements PluginAwareInterface
{
    use PluginAwareTrait;

    /**
     * Render a Resource JSON View Model
     *
     * If View Model is a ResourceJsonModel, determines if it represents a Collection
     * or Resource and if renders it accordingly
     *
     * @param string|\Zend\View\Model\ModelInterface $nameOrModel
     * @param null $values
     * @return string
     */
    public function render($nameOrModel, $values = null)
    {
        if(!$nameOrModel instanceof ResourceJsonModel)
        {
            return parent::render($nameOrModel, $values);
        }

        if($nameOrModel->isResource())
        {
            $resource = $nameOrModel->getPayload();
            return !is_null($this->getPlugin()) ? $this->getPlugin()->renderResource($resource): parent::render($resource);
        }

        if($nameOrModel->isCollection())
        {
            $collection = $nameOrModel->getPayload();
            return $this->getPlugin()->renderCollection($collection);
        }

        return parent::render($nameOrModel, $values);
    }
} 
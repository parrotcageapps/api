<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource;

use Zend\EventManager\Event;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;

/**
 * Class ResourceEvent
 * @package Parrot\API\Resource
 */
class ResourceEvent extends Event
{
    /**
     * Resource Identifier
     *
     * @var string
     */
    protected $identifier;

    /**
     * @var null|Parameters
     */
    protected $queryParams;

    /**
     * @var null|RouteMatch
     */
    protected $routeMatch;

    /**
     * Set Resource Identifier
     *
     * @param string $resourceIdentifier
     */
    public function setResourceIdentifier($resourceIdentifier)
    {
        $this->identifier = $resourceIdentifier;
    }

    /**
     * Get Resource Identifier
     *
     * @return string
     */
    public function getResourceIdentifier()
    {
        return $this->identifier;
    }
    /**
     * @param Parameters $params
     * @return self
     */
    public function setQueryParams(Parameters $params = null)
    {
        $this->queryParams = $params;
        return $this;
    }

    /**
     * @return null|Parameters
     */
    public function getQueryParams()
    {
        return $this->queryParams;
    }

    /**
     * Retrieve a single query parameter by name
     *
     * If not present, returns the $default value provided.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getQueryParam($name, $default = null)
    {
        $params = $this->getQueryParams();
        if (null === $params) {
            return $default;
        }

        return $params->get($name, $default);
    }

    /**
     * @param RouteMatch $matches
     * @return self
     */
    public function setRouteMatch(RouteMatch $matches = null)
    {
        $this->routeMatch = $matches;
        return $this;
    }

    /**
     * @return null|RouteMatch
     */
    public function getRouteMatch()
    {
        return $this->routeMatch;
    }

    /**
     * Retrieve a single route match parameter by name.
     *
     * If not present, returns the $default value provided.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getRouteParam($name, $default = null)
    {
        $matches = $this->getRouteMatch();
        if (null === $matches) {
            return $default;
        }

        return $matches->getParam($name, $default);
    }
} 
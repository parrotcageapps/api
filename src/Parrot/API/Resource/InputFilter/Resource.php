<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\InputFilter;

use HTMLPurifier;
use Traversable;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputInterface;

/**
 * Class Resource
 * @package Parrot\API\Resource\InputFilter
 */
class Resource extends InputFilter
{
    /**
     * @var HtmlPurifier
     */
    protected $htmlPurifier;

    /**
     * Constructor
     *
     * @param HTMLPurifier $htmlPurifier
     */
    public function __construct(HTMLPurifier $htmlPurifier)
    {
        $this->setHtmlPurifier($htmlPurifier);
    }

    /**
     * Add an input to the input filter
     *
     * @param  array|Traversable|InputInterface|InputFilterInterface $input
     * @param  null|string $name
     * @param $disablePurifier
     * @return InputFilter
     */
    public function add($input, $name = null, $disablePurifier = false)
    {
        /**
         * By Default all Inputs will filter against XSS and invalid HTML, attributes and scripts
         */
        $purifier = $this->getHtmlPurifier();
        $input->getFilterChain()->attach(function($value) use ($purifier, $disablePurifier) {
            if($purifier instanceof HTMLPurifier && !$disablePurifier)
            {
                if(is_array($value))
                {
                    foreach($value as $key => $v)
                    {
                        $value[$key] = $purifier->purify($v);
                    }

                    return $value;
                }else{
                    return $purifier->purify($value);
                }
            }

            return $value;
        });

        return parent::add($input, $name);
    }

    /**
     * @param \HTMLPurifier $htmlPurifier
     */
    public function setHtmlPurifier($htmlPurifier)
    {
        $this->htmlPurifier = $htmlPurifier;
    }

    /**
     * @return \HTMLPurifier
     */
    public function getHtmlPurifier()
    {
        return $this->htmlPurifier;
    }
} 
<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Resource\Service;

use Parrot\API\Resource\Resource;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ResourceLocator
 * @package Parrot\API\Resource\Service
 */
class ResourceLocator implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Constructor
     *
     * @param ServiceLocatorInterface $sl
     */
    public function __construct(ServiceLocatorInterface $sl)
    {
        $this->setServiceLocator($sl);
    }

    /**
     * Locates a Resource by it's identifier
     *
     * @param $identifier
     * @return boolean|Resource
     */
    public function __invoke($identifier)
    {
        if($this->getServiceLocator()->has('Config'))
        {
            $config = $this->getServiceLocator()->get('Config');

            if(isset($config['parrot_api_resource_map']))
            {
                if(in_array($identifier, array_keys($config['parrot_api_resource_map'])))
                {
                    // Get Resource from Service Manager
                    $resourceAlias = $config['parrot_api_resource_map'][$identifier];
                    $sm            = $this->getServiceLocator();

                    $resource = ($sm->has($resourceAlias))? $sm->get($resourceAlias) : null;

                    if($resource instanceof Resource)
                    {
                        return $resource;
                    }
                }
            }
        }

        return false;
    }
} 
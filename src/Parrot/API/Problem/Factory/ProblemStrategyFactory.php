<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Problem\Factory;

use Parrot\API\Problem\View\Strategy\ProblemStrategy;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ProblemStrategy
 * @package Parrot\API\Problem\Factory
 */
class ProblemStrategyFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new ProblemStrategy($serviceLocator->get('Parrot\API\Problem\View\Renderer\ProblemRenderer'));
    }
}
<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Problem\View\Strategy;

use Parrot\API\Problem\Problem;
use Parrot\API\Problem\View\Renderer\ProblemRenderer;
use Parrot\API\Problem\View\Model\ProblemModel;
use Zend\View\Strategy\JsonStrategy;
use Zend\View\ViewEvent;

/**
 * Class ProblemStrategy
 * @package Parrot\API\Problem\View\Strategy
 */
class ProblemStrategy extends JsonStrategy
{
    /**
     * @param ProblemRenderer $renderer
     */
    public function __construct(ProblemRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * Detect if we should use the ProblemRenderer based on model type.
     *
     * @param  ViewEvent $e
     * @return null|ProblemRenderer
     */
    public function selectRenderer(ViewEvent $e)
    {
        $model = $e->getModel();

        if (!$model instanceof ProblemModel) {
            return;
        }

        // ProblemModel found
        return $this->renderer;
    }

    /**
     * Inject the response
     *
     * Injects the response with the rendered content, and sets the content
     * type based on the detection that occurred during renderer selection.
     *
     * @param  ViewEvent $e
     */
    public function injectResponse(ViewEvent $e)
    {
        $result = $e->getResult();
        if (!is_string($result)) {
            // We don't have a string, and thus, no JSON
            return;
        }

        $model = $e->getModel();
        if (!$model instanceof ProblemModel) {
            return;
        }

        $problem     = $model->getApiProblem();
        $statusCode  = $this->getStatusCodeFromApiProblem($problem);
        $contentType = 'application/api-problem+json';

        // Populate response
        $response = $e->getResponse();
        $response->setStatusCode($statusCode);
        $response->setContent($result);
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', $contentType);
    }

    /**
     * Retrieve the HTTP status from an Problem object
     *
     * Ensures that the status falls within the acceptable range (100 - 599).
     *
     * @param  Problem $problem
     * @return int
     */
    protected function getStatusCodeFromApiProblem(Problem $problem)
    {
        $status = $problem->httpStatus;

        if ($status < 100 || $status >= 600) {
            return 500;
        }

        return $status;
    }
}
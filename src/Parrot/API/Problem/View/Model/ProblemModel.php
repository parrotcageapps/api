<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Problem\View\Model;

use Parrot\API\Problem\Problem;
use Zend\View\Model\ViewModel;

/**
 * Class ProblemModel
 * @package Parrot\API\Problem\View\Model
 */
class ProblemModel extends ViewModel
{
    /**
     * @var string
     */
    protected $captureTo = 'errors';

    /**
     * @var Problem
     */
    protected $problem;

    /**
     * @var bool
     */
    protected $terminate = true;

    /**
     * @param Problem|null $problem
     */
    public function __construct(Problem $problem = null)
    {
        if ($problem instanceof Problem) {
            $this->setApiProblem($problem);
        }
    }

    /**
     * @param  Problem $problem
     * @return ProblemModel
     */
    public function setApiProblem(Problem $problem)
    {
        $this->problem = $problem;
        return $this;
    }

    /**
     * @return Problem
     */
    public function getApiProblem()
    {
        return $this->problem;
    }
}
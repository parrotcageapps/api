<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Problem\Listener;

use Parrot\API\Problem\Exception\ProblemExceptionInterface;
use Parrot\API\Problem\Problem;
use Parrot\API\Problem\Response\ProblemResponse;
use Parrot\API\Problem\View\Model\ProblemModel;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\Http\Request as HttpRequest;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ModelInterface;

/**
 * Class ProblemListener
 * @package Parrot\API\Problem\Listener
 */
class ProblemListener extends AbstractListenerAggregate
{
    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_RENDER, array($this, 'onRender'), 1000);
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), 100);

        $sharedEvents = $events->getSharedManager();
        $sharedEvents->attach(
            'Zend\Stdlib\DispatchableInterface',
            MvcEvent::EVENT_DISPATCH,
            array($this, 'onDispatch'),
            100
        );
    }

    /**
     * Listen to the render event
     *
     * @param MvcEvent $e
     */
    public function onRender(MvcEvent $e)
    {
        if (!$this->validateErrorEvent($e)) {
            return;
        }

        // Next, do we have a view model in the result?
        // If not, nothing more to do.
        $model = $e->getResult();
        if (!$model instanceof ModelInterface || $model instanceof ProblemModel) {
            return;
        }

        // Marshal the information we need for the API-Problem response
        $httpStatus = $e->getResponse()->getStatusCode();
        $exception  = $model->getVariable('exception');

        if ($exception instanceof \Exception) {
            $apiProblem = new Problem($httpStatus, $exception);
        } else {
            $apiProblem = new Problem($httpStatus, $model->getVariable('message'));
        }

        // Create a new model with the API-Problem payload, and reset
        // the result and view model in the event using it.
        $model = new ProblemModel($apiProblem);
        $e->setResult($model);
        $e->setViewModel($model);
    }

    /**
     * Handle render errors
     *
     * If the event representes an error, and has an exception composed, marshals a Problem
     * based on the exception, stops event propagation, and returns an ProblemResponse.
     *
     * @param  MvcEvent $e
     * @return ProblemResponse
     */
    public function onDispatchError(MvcEvent $e)
    {
        if (!$this->validateErrorEvent($e)) {
            return;
        }

        $exception = $e->getParam('exception');
        if ($exception instanceof ProblemExceptionInterface || $exception instanceof \Exception) {
            $status = $exception->getCode();

            if (0 === $status) {
                $status = 500;
            }
            $problem = new Problem($status, $exception);
        } else {
            return;
        }

        $e->stopPropagation();
        $response = new ProblemResponse($problem);
        $e->setResponse($response);

        return $response;
    }

    /**
     * Handle dispatch
     *
     * It checks if the controller is in our list
     *
     * @param MvcEvent $e
     */
    public function onDispatch(MvcEvent $e)
    {
        $app      = $e->getApplication();
        $services = $app->getServiceManager();

        // Attach the Problem render.error listener
        $events = $app->getEventManager();
        $events->attach($services->get('Parrot\API\Problem\Listener\RenderErrorListener'));
    }

    /**
     * Determine if we have a valid error event
     *
     * @param  MvcEvent $e
     * @return bool
     */
    protected function validateErrorEvent(MvcEvent $e)
    {
        // only worried about error pages
        if (!$e->isError()) {
            return false;
        }

        // and then, only if we have an Accept header...
        $request = $e->getRequest();
        if (!$request instanceof HttpRequest) {
            return false;
        }

        return true;
    }
}
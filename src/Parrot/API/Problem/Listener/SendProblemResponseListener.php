<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Problem\Listener;

use Parrot\API\Problem\Response\ProblemResponse;
use Zend\Mvc\ResponseSender\HttpResponseSender;
use Zend\Mvc\ResponseSender\SendResponseEvent;

/**
 * Class SendProblemResponseListener
 * @package Parrot\API\Problem\Listener
 */
class SendProblemResponseListener extends HttpResponseSender
{
    /**
     * @var bool
     */
    protected $displayExceptions = false;

    /**
     * Set the flag determining whether exception stack traces are included
     *
     * @param  bool $flag
     * @return self
     */
    public function setDisplayExceptions($flag)
    {
        $this->displayExceptions = (bool) $flag;
        return $this;
    }

    /**
     * Are exception stack traces included in the response?
     *
     * @return bool
     */
    public function displayExceptions()
    {
        return $this->displayExceptions;
    }

    /**
     * Send the response content
     *
     * Sets the composed Problem's flag for including the stack trace in the
     * detail based on the display exceptions flag, and then sends content.
     *
     * @param SendResponseEvent $e
     * @return self
     */
    public function sendContent(SendResponseEvent $e)
    {
        $response = $e->getResponse();
        if (!$response instanceof ProblemResponse) {
            return $this;
        }
        $response->getApiProblem()->setDetailIncludesStackTrace($this->displayExceptions());
        return parent::sendContent($e);
    }

    /**
     * Send ApiProblem response
     *
     * @param  SendResponseEvent $event
     * @return self
     */
    public function __invoke(SendResponseEvent $event)
    {
        $response = $event->getResponse();
        if (!$response instanceof ProblemResponse) {
            return $this;
        }

        $this->sendHeaders($event)
             ->sendContent($event);
        $event->stopPropagation(true);
        return $this;
    }
} 
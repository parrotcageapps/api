<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Problem\Exception;

/**
 * Interface ProblemExceptionInterface
 * @package Parrot\API\Problem\Exception
 */
interface ProblemExceptionInterface
{
    /**
     * @return null|array|\Traversable
     */
    public function getAdditionalDetails();

    /**
     * @return string
     */
    public function getProblemType();

    /**
     * @return string
     */
    public function getTitle();
}
<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Problem\Exception;

/**
 * Class InvalidArgumentException
 * @package Parrot\API\Problem\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface {}
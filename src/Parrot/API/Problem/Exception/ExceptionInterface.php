<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API\Problem\Exception;

/**
 * Interface ExceptionInterface
 * @package Parrot\API\Problem\Exception
 */
interface ExceptionInterface {}
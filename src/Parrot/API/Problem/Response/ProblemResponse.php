<?php
/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */
 
 /**
 * CTI Digital
 *
 * @author Jason Brown <j.brown@ctidigital.com>
 */

namespace Parrot\API\Problem\Response;

use Parrot\API\Problem\Problem;
use Zend\Http\Response;

/**
 * Class ProblemResponse
 * @package Parrot\API\Problem\Response
 */
class ProblemResponse extends Response
{
    protected $apiProblem;

    /**
     * @param Problem $apiProblem
     */
    public function __construct(Problem $apiProblem)
    {
        $this->apiProblem = $apiProblem;
        $this->setStatusCode($apiProblem->httpStatus);
        $this->setReasonPhrase($apiProblem->title);
    }

    /**
     * @return ApiProblem
     */
    public function getApiProblem()
    {
        return $this->apiProblem;
    }

    /**
     * Retrieve the content
     *
     * Serializes the composed ApiProblem instance to JSON.
     *
     * @return string
     */
    public function getContent()
    {
        return json_encode($this->apiProblem->toArray(), JSON_UNESCAPED_SLASHES);
    }

    /**
     * Retrieve headers
     *
     * Proxies to parent class, but then checks if we have an content-type
     * header; if not, sets it, with a value of "application/api-problem+json".
     *
     * @return \Zend\Http\Headers
     */
    public function getHeaders()
    {
        $headers = parent::getHeaders();

        if (!$headers->has('content-type')) {
            $headers->addHeaderLine('content-type', 'application/api-problem+json');
        }
        return $headers;
    }
} 
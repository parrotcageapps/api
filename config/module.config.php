<?php

/**
 * Parrot Framework
 *
 * @author Jason Brown <jason.brown@parrotcageapps.com>
 */

namespace Parrot\API;

return array(
    'router' => array(
        'routes' => array(
            'api' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/api',
                ),
                'may_terminate' => false,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/:resource[/:id]',
                            'defaults' => array(
                                'controller' => 'Parrot\API\Resource\Controller\Resource',
                            )
                        ),
                    ),
                ),
            ),
        )
    ),
    'service_manager' => array(
        'invokables' => array(
            'Parrot\API\Resource\Listener\RestParameterListener'     => 'Parrot\API\Resource\Listener\RestParameterListener',
            'Parrot\API\Resource\Listener\MethodNotAllowedListener'  => 'Parrot\API\Resource\Listener\MethodNotAllowedListener',
            'Parrot\API\Resource\Listener\ContentValidationListener' => 'Parrot\API\Resource\Listener\ContentValidationListener'
        ),
        'factories' => array(
            'Parrot\API\Resource'                                       => 'Parrot\API\Resource\Factory\ResourceFactory',
            'Parrot\API\Service\ResourceLocator'                        => 'Parrot\API\Resource\Factory\ResourceLocatorFactory',
            'Parrot\API\Resource\Cache'                                 => 'Parrot\API\Resource\Factory\ResourceCacheFactory',
            'Parrot\API\Resource\View\Strategy\ResourceJsonHalStrategy' => 'Parrot\API\Resource\Factory\ResourceJsonHalStrategyFactory',
            'Parrot\API\Resource\Listener\ResponseCacheListener'        => 'Parrot\API\Resource\Factory\ResponseCacheListenerFactory',
            'Parrot\API\Problem\Listener\ProblemListener'               => 'Parrot\API\Problem\Factory\ProblemListenerFactory',
            'Parrot\API\Problem\Listener\SendProblemResponseListener'   => 'Parrot\API\Problem\Factory\SendProblemResponseListenerFactory',
            'Parrot\API\Problem\Listener\RenderErrorListener'           => 'Parrot\API\Problem\Factory\RenderErrorListenerFactory',
            'Parrot\API\Problem\View\Renderer\ProblemRenderer'          => 'Parrot\API\Problem\Factory\ProblemRendererFactory',
            'Parrot\API\Problem\View\Strategy\ProblemStrategy'          => 'Parrot\API\Problem\Factory\ProblemStrategyFactory'
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            /**
             * Replace Standard JSON Strategy with Parrot Resource JSON Strategy
             */
            'Parrot\API\Resource\View\Strategy\ResourceJsonHalStrategy',
        ),
        'display_exceptions' => false,
    ),
    'parrot_api' => array(
        'resource_cache' => array(
            'adapter' => 'Zend\Cache\Storage\Adapter\Memory',
            'options' => array(
                'namespace' => 'parrot_api_resource'
            ),
            'plugins' => array(
                'Zend\Cache\Storage\Plugin\Serializer'
            ),
        ),
    ),
    'parrot_api_resource_map' => array(
        /**
         * key/value array specifying a resource identifier and a matching Resource service alias
         * 'user' => 'Parrot\API\Resource\User'
         */
    ),
    /**
     * HTML Purifier Config for default Resource Input Filter
     */
    'soflomo_purifier' => array(
        'config' => array(
            'HTML.AllowedElements'     => 'h1,h2,h3,h4,h5,h6,p,ul,ol,li,i,b,a',
            'Attr.AllowedClasses'      => '',
            'HTML.AllowedAttributes'   => '',
            'AutoFormat.RemoveEmpty'   => true,
        )
    ),
);
